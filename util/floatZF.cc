// floatZF.cc

#include <cassert>
#include <cmath>
#include <floatZF.hh>

#ifndef FLOATZF_LINE_LENGTH
#   define FLOATZF_LINE_LENGTH .1
#endif

#ifndef FLOATZF_PRECISION
#   define FLOATZF_PRECISION 1e-5
#endif

using namespace std;

// ************************************************************ floatZF

floatZF::floatZF(Host *fh)
: fh(fh)  
{}

void floatZF::clear()
{
    zeros.clear();
    poles.clear();
}

void floatZF::count_in(const Domain &d)
{
    if ( DEBUG )
        cerr.precision(14);

    for ( Domain::const_iterator i = d.begin(); i != d.end(); i++ )
    {
        if ( DEBUG )
        {
            cerr << "floatZF (" 
                 << max_rect << ":" << line_length << ":" << precision << ":" << subdivisions 
                 << ") searching in " << (*i) << endl;
        }
        count_in(*i);
    }
}

int floatZF::count_in(const Box &b)
{
    double I = 0;
    
    if ( DEBUG )
        cerr << "CR: " << b << endl;

    if ( max_rect > 0 && b.max_side() > max_rect )
    {
        if ( b.xmax - b.xmin > max_rect )
        {
            double m = .5*(b.xmax + b.xmin);
            count_in(Box(b.xmin,m,b.ymin,b.ymax));
            count_in(Box(m,b.xmax,b.ymin,b.ymax));
        }
        else
        {
            double m = .5*(b.ymax + b.ymin);
            count_in(Box(b.xmin,b.xmax,b.ymin,m));
            count_in(Box(b.xmin,b.xmax,m,b.ymax));
        }
        return 0;
    }
    else
    {
        line_length_x = line_length_y = line_length;

        if ( subdivisions >= 0 )
        {
            double x, y,
                   X = line_length_x, 
                   Y = line_length_y;
            double d = (int) pow(2.0,subdivisions) - .5;
            x = (b.xmax - b.xmin)/d;
            y = (b.ymax - b.ymin)/d;
            X = (X <= 0 || x < X) ? x : X;
            Y = (Y <= 0 || y < Y) ? y : Y;
            line_length_x = X;
            line_length_y = Y;
        }

        I += count_line(COMPLEX(b.xmin,b.ymin), COMPLEX(b.xmax,b.ymin));
        I += count_line(COMPLEX(b.xmax,b.ymin), COMPLEX(b.xmax,b.ymax));
        I += count_line(COMPLEX(b.xmax,b.ymax), COMPLEX(b.xmin,b.ymax));
        I += count_line(COMPLEX(b.xmin,b.ymax), COMPLEX(b.xmin,b.ymin));

        int c = (int)rint(I/TWO_PI);

        if ( c != 0 )
        {
            if ( b.max_side() > precision )
            {
                int c1, c2;
                if ( b.xmax - b.xmin > b.ymax - b.ymin )
                {
                    double m = .5*(b.xmax + b.xmin);
                    c1 = count_in(Box(b.xmin,m,b.ymin,b.ymax));
                    c2 = count_in(Box(m,b.xmax,b.ymin,b.ymax));
                }
                else
                {
                    double m = .5*(b.ymax + b.ymin);
                    c1 = count_in(Box(b.xmin,b.xmax,b.ymin,m));
                    c2 = count_in(Box(b.xmin,b.xmax,m,b.ymax));
                }

                if ( c1 + c2 != c )
                {
                    if ( DEBUG )
                        cerr << "FR: " << b << "  " << c1 << " + " << c2 << " != " << c << endl;
                    E_WRONG_COUNT++;
                }
            }
            else
            {
                if ( c > 0 )
                {
                    if ( DEBUG )
                        cerr << "ZZ: " << b << endl;
                    zeros.push_back(b.middle());
                }
                else
                    poles.push_back(b.middle());
            }
        }
        return c;
    }
}

const double floatZF::count_line(const COMPLEX &z1, const COMPLEX &z2) const
{
    bool divide = false;

    if ( z1.real() == z2.real() ) // vertical line
    {
        if ( abs(z1.imag() - z2.imag()) > line_length_y )
            divide = true;
    }
    else
    {                             // horizontal line
        if ( abs(z1.real() - z2.real()) > line_length_x )
            divide = true;
    }
    if ( divide )
    {
        COMPLEX m = .5*(z1 + z2);
        return count_line(z1,m) + count_line(m,z2);
    }
    else
        return arg_diff(fh->f(z2),fh->f(z1));
}

const double floatZF::arg_diff(const COMPLEX &v2, const COMPLEX &v1) const
{
    assert( v1 != 0.0 && v2 != 0.0 );

    double d;
    if ( v1.real() < 0 && v2.real() < 0 )
        d = arg(-v2) - arg(-v1);
    else 
        d = arg(v2) - arg(v1);

    if ( d > PI )
        d -= TWO_PI;
    if ( d < -PI )
        d += TWO_PI;
    
    return d;
}

// static 
void floatZF::set_config(const double &max_rect, const double &line_length, const double &precision,
                         const int &subdivisions)
{
    floatZF::max_rect = max_rect;
    floatZF::line_length = line_length;
    floatZF::precision = precision;
    floatZF::subdivisions = subdivisions;

    if ( floatZF::line_length <= 0 && floatZF::subdivisions < 0 )
        floatZF::subdivisions = 0;

    if ( floatZF::precision <= 0 )
        floatZF::precision = FLOATZF_PRECISION;
}



long floatZF::E_WRONG_COUNT = 0;
double floatZF::max_rect = FLOATZF_MAX_RECT;
double floatZF::line_length = FLOATZF_LINE_LENGTH;
double floatZF::precision = FLOATZF_PRECISION;
int floatZF::subdivisions = FLOATZF_SUBDIVISIONS;
bool floatZF::DEBUG = false;


