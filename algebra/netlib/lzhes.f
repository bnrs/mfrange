C http://www.netlib.org/toms/
      SUBROUTINE LZHES(N, A, NA, B, NB, X, NX, WANTX)                   LZH   10
C THIS SUBROUTINE REDUCES THE COMPLEX MATRIX A TO UPPER
C HESSENBERG FORM AND REDUCES THE COMPLEX MATRIX B TO
C TRIANGULAR FORM
C INPUT PARAMETERS..
C N   THE ORDER OF THE A AND B MATRICES
C A   A COMPLEX MATRIX
C NA  THE ROW DIMENSION OF THE A MATRIX
C B   A COMPLEX MATRIX
C NB  THE ROW DIMENSION OF THE B MATRIX
C NX  THE ROW DIMENSION OF THE X MATRIX
C WANTX A LOGICAL VARIABLE WHICH IS SET TO .TRUE. IF
C       THE EIGENVECTORS ARE WANTED. OTHERWISE IT SHOULD
C     BE SET TO .FALSE.
C OUTPUT PARAMETERS..
C A  ON OUTPUT A IS AN UPPER HESSENBERG MATRIX, THE
C    ORIGINAL MATRIX HAS BEEN DESTROYED
C B  AN UPPER TRIANGULAR MATRIX, THE ORIGINAL MATRIX
C    HAS BEEN DESTROYED
C X  CONTAINS THE TRANSFORMATIONS NEEDED TO COMPUTE
C    THE EIGENVECTORS OF THE ORIGINAL SYSTEM
      COMPLEX Y, W, Z, A(NA,N), B(NB,N), X(NX,N)
      REAL C, D
      LOGICAL WANTX
      NM1 = N - 1
C REDUCE B TO TRIANGULAR FORM USING ELEMENTARY
C TRANSFORMATIONS
      DO 80 I=1,NM1
        D = 0.00
        IP1 = I + 1
        DO 10 K=IP1,N
          Y = B(K,I)
          C = ABS(REAL(Y)) + ABS(AIMAG(Y))
          IF (C.LE.D) GO TO 10
          D = C
          II = K
   10   CONTINUE
        IF (D.EQ.0.0) GO TO 80
        Y = B(I,I)
        IF (D.LE.ABS(REAL(Y))+ABS(AIMAG(Y))) GO TO 40
C MUST INTERCHANGE
        DO 20 J=1,N
          Y = A(I,J)
          A(I,J) = A(II,J)
          A(II,J) = Y
   20   CONTINUE
        DO 30 J=I,N
          Y = B(I,J)
          B(I,J) = B(II,J)
          B(II,J) = Y
   30   CONTINUE
   40   DO 70 J=IP1,N
          Y = B(J,I)/B(I,I)
          IF (REAL(Y).EQ.0.0 .AND. AIMAG(Y).EQ.0.0) GO TO 70
          DO 50 K=1,N
            A(J,K) = A(J,K) - Y*A(I,K)
   50     CONTINUE
          DO 60 K=IP1,N
            B(J,K) = B(J,K) - Y*B(I,K)
   60     CONTINUE
   70   CONTINUE
        B(IP1,I) = (0.0,0.0)
   80 CONTINUE
C INITIALIZE X
      IF (.NOT.WANTX) GO TO 110
      DO 100 I=1,N
        DO 90 J=1,N
          X(I,J) = (0.0,0.0)
   90   CONTINUE
        X(I,I) = (1.0,0.00)
  100 CONTINUE
C REDUCE A TO UPPER HESSENBERG FORM
  110 NM2 = N - 2
      IF (NM2.LT.1) GO TO 270
      DO 260 J=1,NM2
        JM2 = NM1 - J
        JP1 = J + 1
        DO 250 II=1,JM2
          I = N + 1 - II
          IM1 = I - 1
          IMJ = I - J
          W = A(I,J)
          Z = A(IM1,J)
          IF (ABS(REAL(W))+ABS(AIMAG(W)).LE.ABS(REAL(Z))
     *     +ABS(AIMAG(Z))) GO TO 140
C MUST INTERCHANGE ROWS
          DO 120 K=J,N
            Y = A(I,K)
            A(I,K) = A(IM1,K)
            A(IM1,K) = Y
  120     CONTINUE
          DO 130 K=IM1,N
            Y = B(I,K)
            B(I,K) = B(IM1,K)
            B(IM1,K) = Y
  130     CONTINUE
  140     Z = A(I,J)
          IF (REAL(Z).EQ.0.0 .AND. AIMAG(Z).EQ.0.0) GO TO 170
          Y = Z/A(IM1,J)
          DO 150 K=JP1,N
            A(I,K) = A(I,K) - Y*A(IM1,K)
  150     CONTINUE
          DO 160 K=IM1,N
            B(I,K) = B(I,K) - Y*B(IM1,K)
  160     CONTINUE
C TRANSFORMATION FROM THE RIGHT
  170     W = B(I,IM1)
          Z = B(I,I)
          IF (ABS(REAL(W))+ABS(AIMAG(W)).LE.ABS(REAL(Z))
     *     +ABS(AIMAG(Z))) GO TO 210
C MUST INTERCHANGE COLUMNS
          DO 180 K=1,I
            Y = B(K,I)
            B(K,I) = B(K,IM1)
            B(K,IM1) = Y
  180     CONTINUE
          DO 190 K=1,N
            Y = A(K,I)
            A(K,I) = A(K,IM1)
            A(K,IM1) = Y
  190     CONTINUE
          IF (.NOT.WANTX) GO TO 210
          DO 200 K=IMJ,N
            Y = X(K,I)
            X(K,I) = X(K,IM1)
            X(K,IM1) = Y
  200     CONTINUE
  210     Z = B(I,IM1)
          IF (REAL(Z).EQ.0.0 .AND. AIMAG(Z).EQ.0.0) GO TO 250
          Y = Z/B(I,I)
          DO 220 K=1,IM1
            B(K,IM1) = B(K,IM1) - Y*B(K,I)
  220     CONTINUE
          B(I,IM1) = (0.0,0.0)
          DO 230 K=1,N
            A(K,IM1) = A(K,IM1) - Y*A(K,I)
  230     CONTINUE
          IF (.NOT.WANTX) GO TO 250
          DO 240 K=IMJ,N
            X(K,IM1) = X(K,IM1) - Y*X(K,I)
  240     CONTINUE
  250   CONTINUE
        A(JP1+1,J) = (0.0,0.0)
  260 CONTINUE
  270 RETURN
      END
