// stopper.hh

#ifndef __stopper_hh
#define __stopper_hh

#include <sys/times.h>
#include <unistd.h>

/// Simple class for stopping time
class stopper
{
    public:
        stopper()
        {
            reset();
        }
        
        void reset()
        {
            times(&start);
        }
        
        const float user_seconds()
        {
            struct tms current;
            times(&current);

            return ((float) current.tms_utime - start.tms_utime)/sysconf(_SC_CLK_TCK);
        }

        const float system_seconds()
        {
            struct tms current;
            times(&current);

            return ((float) current.tms_stime - start.tms_stime)/sysconf(_SC_CLK_TCK);
        }

    private:
        struct tms start;
};

#endif
