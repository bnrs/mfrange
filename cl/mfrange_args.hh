// mfrange_args.hh

#ifndef __mfrange_args_hh
#define __mfrange_args_hh

#include <string>
#include <sstream>
#include <vector>
#include <set>
#include <config.hh>
#include <tclap/CmdLine.h>

/// Parse command line args for mfrange command line application

class mfrange_args
{
    public:
        mfrange_args();
        mfrange_args(int argc, char *argv[]);

        std::string    
            matrix_function,    ///< Filename of the function to use
            J_matrix,           ///< Filename of the J-function to use
            range_type,         ///< Contains "bnr", "ev", "bds", ...
            color,              ///< Color to use in the form rgb, where r,b,g are each one of x or 0
            symbol_color;
            
        unsigned  
            resolution;         ///< Output resolution

        COMPLEX   
            lambda,             ///< Only used in BDS
            z;

        int 
            png_width,
            png_height,
            symbol_size,
            bdsbnr_xres,
            bdsbnr_yres,
            bdsbnr_max_iter;

        char
            symbol_style;

        std::vector< std::set< int > >
            decomposition;      ///< The decomposition as a vector

        std::vector< double >
            gbnr_p; ///< The Gershgorin numbers p_1,...,p_n

        std::set< int >
            sub,
            schur;
            
        float
            xmin, xmax, ymin, ymax,
            bdsbnr_eps;

        bool
            grid,
            classical_colors,
            no_sss,
            no_append,
            eval,
            companion,
            xy_file,
            bds_mark_zero,
            ses;

        void reset();

        /// To be called before the variables can be accessed
        void parse_args(int argc, char *argv[]);

        const std::string parse_error();

//    private:
        std::string
            decomposition_str,  ///< A string of the form "k1-k2-...-kn"
            bounds_str,
            bdsbnr_str,
            png_size_str,
            sub_str,
            schur_str,
            gbnr_p_str,
            zf_config_str;

        /// Fill the variables
        void post_process();
};

#endif
