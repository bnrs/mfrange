// ComplexVBector.cc

#include <ComplexVector.hh>
#include <cassert>

using namespace std;

// **************************************************************** STRUCTORS 

ComplexVector::ComplexVector() 
: entry(NULL)
{
    dim = -1;
}

ComplexVector::ComplexVector(const int &d, const COMPLEX *entry)
: entry(NULL)
{ 
    ASSERT(d >= 0, "");
    dim = d; 
    entry_init(entry);
}

ComplexVector::ComplexVector(const ComplexVector &other)
: entry(NULL)
{
    dim = other.dim;
    entry_init(other.entry);
}  

ComplexVector::~ComplexVector()
{ 
    delete[] entry;
}

const ComplexVector &ComplexVector::operator = (const ComplexVector other)
{
    dim = other.dim;
    entry_init(other.entry);
    return (*this);
}  

// **************************************************************** ==

const bool ComplexVector::operator == (const ComplexVector &other) const
{
    if ( dim != other.dim ) 
        return false;
    for ( int i = 0; i < dim; ++i ) 
        if ( entry[i] != other.entry[i] ) 
            return false;
    return true;
}  

const bool ComplexVector::operator != (const ComplexVector &other) const
{
    return !( (*this) == other );
}

const int &ComplexVector::dimension() const
{
    return dim;
}

const bool ComplexVector::is_empty() const
{
    return ( dim == -1 );
}

const bool ComplexVector::isnt_empty() const
{
    return !is_empty();
}

const bool ComplexVector::is_null() const
{
    return is_empty();
}

const bool ComplexVector::isnt_null() const
{
    return !is_empty();
}

// *********************************************************************** () 

COMPLEX &ComplexVector::operator () (const int &k)
{
    ASSERT(0 <= k && k < dim, SHOW_VAR(k) AND_SHOW_VAR(*this));
    return entry[k];
}

const COMPLEX &ComplexVector::operator () (const int &k) const
{
    ASSERT(0 <= k && k < dim, SHOW_VAR(k) AND_SHOW_VAR(*this));
    return entry[k];
}

const ComplexVector ComplexVector::sub(const int &k1, const int &k2)
{
    ASSERT(0 <= k1 && k1 <= k2 && k2 < dim,
           SHOW_VAR(k1) AND_SHOW_VAR(k2) AND_SHOW_VAR(*this));
    ComplexVector result(k2 - k1 + 1);
    entry_copy(result.entry, &entry[k1], result.dim);
    return result;
}  

const ComplexVector ComplexVector::operator () (const int &k1, const int &k2)
{
    return this->sub(k1,k2);
}

// *********************************************************************** FUNCTIONS

const ComplexVector &ComplexVector::operator *= (const COMPLEX &scalar)
{
    ASSERT(isnt_empty(), "");
    for ( int i = 0; i < dim; ++i ) 
        entry[i] *= scalar;
    return (*this);
}  

const ComplexVector ComplexVector::operator * (const COMPLEX &scalar) const
{
    ASSERT(isnt_empty(), "");
    ComplexVector result (*this);
    return result *= scalar;
}

const ComplexVector &ComplexVector::operator /= (const COMPLEX &scalar)
{
    ASSERT(isnt_empty(), "");
    ASSERT(scalar != COMPLEX(0), "");
    return (*this) *= (COMPLEX(1)/scalar);
}  

const ComplexVector ComplexVector::operator / (const COMPLEX &scalar) const
{
    ASSERT(isnt_empty(), "");
    ASSERT(scalar != COMPLEX(0), "");
    ComplexVector result (*this);
    return result /= scalar;
}  

const ComplexVector &ComplexVector::operator += (const ComplexVector &other)
{
    ASSERT(dim == other.dim, SHOW_VAR(*this)AND_SHOW_VAR(other));
    
    for ( int i = 0; i < dim; ++i )
        entry[i] += other.entry[i];
    
    return *this;
}


const COMPLEX ComplexVector::operator | (const ComplexVector &other) const
{
    ASSERT(isnt_empty(), "");
    ASSERT(dim == other.dim, SHOW_VAR(*this) AND_SHOW_VAR(other));
    COMPLEX result(0);
    for ( int i = 0; i < dim; ++i ) 
        result += entry[i] * conj(other.entry[i]);
    return result;
}

const double ComplexVector::norm2() const
{
    ASSERT(isnt_empty(), "");
    if ( dim == 0 )
        return 0;
    
    double n2 = 0;
    for ( int k = 0; k < dim; ++k )
        n2 += std::norm(entry[k]);
    
    return n2;
}

const double ComplexVector::norm() const
{
    ASSERT(isnt_empty(), "");
    return sqrt(norm2());
}

// *********************************************************************** INTERNALS

void ComplexVector::entry_copy (COMPLEX *dest, const COMPLEX *src, const int &count)
{
    for (int i = 0; i < count; ++i)
        dest[i] = src[i];
}

void ComplexVector::entry_init(const COMPLEX *entry)
{
    ASSERT(dim >= 0, "");
    
    if ( this->entry != NULL )
        delete[] this->entry;
    
    if ( dim == 0 )
        this->entry = NULL;
    else
    {
        this->entry = new COMPLEX[dim];

        if ( entry == NULL )
            for ( int i = 0; i < dim; i++ )
                this->entry[i] = 0.0;
        else
            entry_copy(this->entry, entry, dim);
    }
}


// *********************************************************************** IO

ostream &operator << (ostream &out, const ComplexVector &v)
{
    if ( v.is_empty() )
        out << "(nil)";
    else if ( v.dimension() == 0 )
        out << "{0}";
    else
    {
        out << "(" << v(0);
        for (int i = 1; i < v.dimension(); ++i) 
            out << "," << v(i);
        out << ")";
    }

    return out;
}  

istream &operator >> (istream &in, ComplexVector &v)
{
    if ( v.is_empty() || v.dimension() == 0 )
        return in;

    while ( in.get() != '(' );
    if ( !( in >>  v(0) ) ) 
    {
        in.setstate(ios::failbit);
        return in;
    }
    
    for ( int k = 1; k < v.dimension(); ++k ) 
    {
        while ( in.get() != ',' );
        if ( ! ( in >> v(k) ) ) 
        {
            in.setstate(ios::failbit);
            return in;
        }
    }  
    while ( in.get() != ')' );
    return in;
}  
