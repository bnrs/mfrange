// MatrixFunction.hh

#ifndef __MatrixFunction_hh
#define __MatrixFunction_hh

#include <string>
#include <iostream>
#include <set>
#include <ComplexMatrix.hh>
#include <floatZF.hh>

/// Purely virtual class representing a function \f$\F : \mathbb{C} \to M_m(\mathbb{C})\f$
/**
\em Remark. The spectrum of a matrix function \f$\F : \C\supset\Omega \to M_m(\C)\f$
is the set
\f[
\sgp(\F) := \{\lambda \in \C : \det \F(\lm) = 0\}.
\f]
Let \f$k_1,\dots,k_n\f$ be integers such that \f$k_1 + \cdots + k_n = m\f$,
and let \f$\FF = (F_{ij})_{i,j=1}^n\f$ be the block operator representation
of \f$\F\f$ with respect to the composition 
\f$\C^m = \C^{k_1} \times \cdots \times \C^{k_n} =: \H\f$.
Then, for \f$x = (x_1,\dots,x_n) \in \H\f$, we have the matrix function 
\f$\FF_x : z \mapsto ((F_{ij}(z)x_j,x_i)_{i,j = 1}^n\f$.
The spectrum of \f$\FF_x\f$ will be called \em block-spectrum of \f$\F\f$ 
with respect to \f$x\f$. (Note that the decomposition is implicitly given
by \f$x\f$.)
*/

class MatrixFunction : public floatZF::Host
{
    public:
        MatrixFunction();
        virtual ~MatrixFunction() {}
        
        /// Defined in MatrixFunctionIO.cc
        static MatrixFunction *read(std::istream &in);
        static MatrixFunction *read(const std::string &filename);

        /// Set the domain 
        void set_domain(const Domain &d);

        /// Add \p d to the domain list
        void add_domain(const Domain &d);

        /// Get the domain
        const Domain &get_domain() const { return domain; }

        /// Error at last evaluation?
        const bool &get_eval_error() const { return eval_error; }

/// @name Purely virtual
//@{
        /// Give the function a name
        virtual const std::string type() const = 0;

        /// Write to a stream
        virtual void write(std::ostream &out) const = 0;

        /// The dimension of the matrix function
        virtual const int dimension() const = 0;

        /// Evaluate \f$\F(z)\f$
        virtual const ComplexMatrix eval(const COMPLEX &z) = 0;

        /// Return a pointer to a sub-MatrixFunction 
        virtual MatrixFunction *sub_function(const std::set< int > &K) = 0;
//@}
        
/// @name Virtual
//@{
        /// Evaluate \f$\F_{ij}(z)\f$
        virtual const COMPLEX eval(const int &i, const int &j, const COMPLEX &z);

        /// The block spectrum of \f$\F\f$ with respect to \f$x\f$ and \f$y\f$.
        /**
        If this function is not overridden in a child class, ZeroFinder (using
        the argument principle) is used to find the zeros of \f$\FF_x\f$ inside
        the rectangular domain \f$\Omega\f$.
        */
        virtual const std::vector< COMPLEX > block_eigenvalues
            (const std::vector< std::set< int > > D, 
             const std::vector< ComplexVector > &x, const std::vector< ComplexVector > &y);
        
        const std::vector< COMPLEX > block_eigenvalues
            (const std::vector< std::set< int > > D, const std::vector< ComplexVector > &x);

        /// The spectrum of \f$F\f$
        virtual const std::vector< COMPLEX > eigenvalues();
//@}

    protected:
        Domain domain;
        bool eval_error;
        std::vector< std::set< int > > D;
        std::vector< ComplexVector > x, y;

        const COMPLEX f(const COMPLEX &z);
};

std::ostream &operator << (std::ostream &in, const MatrixFunction &F);

#endif
