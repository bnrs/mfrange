// mfrange.cc

#include <iostream>
#include <fstream>

#include <cassert>
#include <util.hh>
#include <mfrange_args.hh>
#include <fPlot.hh>
#include <MFMatrix.hh>
#include <MFParser.hh>
#include <MFPolynomial.hh>
#include <MFSchurComplement.hh>
#include <MFCompanion.hh>
#include <stopper.hh>

#define DEFAULT_RESOLUTION 100
#define DEFAULT_PNG_WIDTH 800
#define DEFAULT_PNG_HEIGHT 800

#define WARNING(MSG) { cerr << "\a********** WARNING: " << MSG << endl; }
#define ERROR(MSG)   { cerr << "\a********** ERROR: " << MSG << endl; had_error = true; }
#define FATAL(MSG)   { cerr << "\a********** FATAL: " << MSG << endl; exit(1); }

#define SSS_COLOR 230,230,230

using namespace std;

#include <Ranges.hh>

void Ranges::info()
{
    cout << "Ranges::info()" << endl;
    cout << "--------------" << endl << endl;
    
    if ( F != NULL && F->type() == "Matrix" )
    {
        cout << "Gershgorin" << endl;

        ComplexMatrix A = F->eval(0), B;
        int n = decomposition.size();

        vector<DOUBLE> P;
        if ( P.empty() )
            for ( int i = 0; i < n; ++i )
                P.push_back(1.0);

        DOUBLE _R, _C;
        for ( int i = 0; i < n; ++i )
        {
            _R = 0;
            for ( int j = 0; j < n; ++j )
                if ( i != j )
                    _R += P[j]*((ComplexMatrix)A.sub(decomposition[i],decomposition[j])).norm();
            _R /= P[i];

            _C = 0;
            for ( int j = 0; j < n; ++j )
                if ( i != j )
                    _C += ((ComplexMatrix)A.sub(decomposition[j],decomposition[i])).norm()/P[j];
            _C *= P[i];

            set<int> dR;
            for ( int j = 0; j < n; ++j )
                if ( j != i )
                    dR.insert(decomposition[j].begin(),decomposition[j].end());

            cout << "R_" << i + 1 << " = " << _R << " / " << sqrt(2.0)*((ComplexMatrix)A.sub(decomposition[i],dR)).norm() << endl;
            cout << "C_" << i + 1 << " = " << _C << endl; // " / " << ((ComplexMatrix)A.sub(dR,decomposition[i])).norm() << endl;

        }
    }
}


void plot_spheres(fPlot &p, const vector< COMPLEX > &z, const vector< DOUBLE > &r)
{
    bool plot;
    for ( unsigned i = 0; i < z.size(); ++i )
    {
        plot = true;
        for ( unsigned j = 0; j < i; ++j )
            if ( abs(z[i] - z[j]) + r[i] < r[j] )
            {
                plot = false;
                break;
            }
        if ( plot )
            p.sphere(z[i].real(), z[i].imag(), r[i], true);
    }
}

int main(int argc, char *argv[])
{
    bool had_error = false;

    bool decomposition_allowed = false,
         need_resolution = false;

    mfrange_args a;
    a.parse_args(argc, argv);

    MatrixFunction *F = MatrixFunction::read(a.matrix_function);

    if ( F == NULL )
    {
        FATAL("Error reading from " << a.matrix_function)
    }

    ComplexMatrix *J = NULL;
    if ( !a.J_matrix.empty() )
    {
        MatrixFunction *JF = MatrixFunction::read(a.J_matrix);
        if ( JF == NULL )
            FATAL("Error reading J-matrix from " << a.J_matrix)

        if ( JF->type() != "Matrix" )
            ERROR("J-matrix must be a matrix")
        else
            J = new ComplexMatrix(JF->eval(0));
    }

    if ( F == NULL )
        FATAL("Error reading matrix function from " << a.matrix_function)

    if ( F->type() == "Parser" )
    {
        string e = ((MFParser *)F)->get_parse_error();
        if ( !e.empty() )
        {
            FATAL("Error parsing file:\n" << e);
        }
    
        if ( F->get_domain().empty() && !a.bounds_str.empty() )
            F->add_domain(Box(a.xmin,a.xmax,a.ymin,a.ymax));
    }

    if ( a.range_type == "ev" )
    {
        decomposition_allowed = false;
        need_resolution = false;
    }
    else if ( a.range_type == "nr" )
    {
        decomposition_allowed = false;
        need_resolution = true;
    }
    else if ( a.range_type == "bnr" )
    {
        decomposition_allowed = true;
        if ( a.bdsbnr_str.empty() )
            need_resolution = true;
    }
    else if ( a.range_type == "bds" )
    {
        decomposition_allowed = true;
        need_resolution = true;
    }
    else if ( a.range_type == "gbnr" )
    {
        decomposition_allowed = true;
        need_resolution = true;
    }
    else if ( a.range_type == "schur" )
    {
        decomposition_allowed = true;
        need_resolution = true;
    }
    else if ( a.range_type == "info" )
    {
        decomposition_allowed = true;
        need_resolution = false;
    }
    
    if ( a.png_width <= 0 || a.png_height <= 0 )
    {
        a.png_width = DEFAULT_PNG_WIDTH;
        a.png_height = DEFAULT_PNG_HEIGHT;
    }

// CONSTRUCTION OF output_filename
// -------------------------------

    string output_filename;

    if ( !a.sub.empty() )
        output_filename += ".sub-" + a.sub_str;

    if ( a.companion )
        output_filename += ".cp";

    if ( !a.schur.empty() )
        output_filename += ".sc-" + a.schur_str;

    if ( a.eval )
        output_filename += ".z=" + util::to_string(a.z);

    if ( a.range_type == "bnr" && !a.bdsbnr_str.empty() )
        output_filename += ".bdsbnr-" + a.bdsbnr_str;
    else
        output_filename += "." + a.range_type;

    if ( a.range_type == "gbnr" && !a.gbnr_p.empty() )
        output_filename += "-" + a.gbnr_p_str;

    if ( decomposition_allowed )
    {
        output_filename += ".d-" + a.decomposition_str;
    }

    if ( a.range_type == "bds" )
        output_filename += "" + util::to_string(a.lambda);

    if ( !a.J_matrix.empty() )
        output_filename += ".J=" + a.J_matrix;

    // Bounds and png size are not interesting here:
    string xy_filename = a.matrix_function + output_filename + ".xy"; 


    if ( !a.bounds_str.empty() )
        output_filename = "." + a.bounds_str + output_filename;

    output_filename = a.matrix_function + output_filename;

    vector< fPlot > P;
    vector< string > png_filename;
    vector< fPlot >::iterator p;    // p = P.begin() by default.

// CONSISTENCY CHECK
// -----------------

    vector<COMPLEX> e_xy;
    if ( a.xy_file )
    {
        ifstream xy_in(xy_filename.c_str());
        if (xy_in)
        {
            float x[2];
            while ( xy_in.good() )
            {
                xy_in.read((char *)&x, 2*sizeof(float));
                if ( xy_in.good() )
                    e_xy.push_back(COMPLEX(x[0],x[1]));
            }
            xy_in.close();
        }
    }

    if ( !decomposition_allowed && !a.decomposition.empty() )
    {
        WARNING("Ignoring decomposition.");
        a.decomposition.clear();
    }

    if ( need_resolution && a.resolution <= 0 && e_xy.empty() )
    {
        WARNING("Using default resolution " << DEFAULT_RESOLUTION << ".");
        a.resolution = DEFAULT_RESOLUTION;
    }

    if ( !need_resolution && a.resolution > 0 )
        WARNING("Ignoring resolution.");
    
    if ( a.no_sss && a.schur.empty() )
        WARNING("Ignoring -no_sss.");

    if ( !a.sub.empty() )
    {
        for ( set< int >::const_iterator i = a.sub.begin(); i != a.sub.end(); ++i )
            if ( !( 0 <= (*i) && (*i) < F->dimension() ) )
                ERROR("Invalid sub index: " << ((*i) + 1) << ".");

        F = F->sub_function(a.sub);
    }
    
    if ( a.companion )
    {
        if ( F->type() != "Polynomial" )
            FATAL("--companion needs an operator polynomial as input.");

        F = new MFPolynomial(((MFPolynomial *) F)->companion());
    }
    
    if ( !a.schur.empty() )
    { 
        for ( set< int >::const_iterator i = a.schur.begin(); i != a.schur.end(); ++i )
            if ( !( 0 <= (*i) && (*i) < F->dimension() ) )
                ERROR("Invalid schur index: " << ((*i) + 1));

        if ( F->get_domain().empty() )
        {
            if ( Box(a.xmin,a.xmax,a.ymin,a.ymax).has_interior() )
                F->add_domain(Box(a.xmin,a.xmax,a.ymin,a.ymax));
            else if ( !a.eval )
                ERROR("Schur complement for a function without domain needs bounds (-b).");
        }

        F = new MFSchurComplement(F, a.schur);
    }    

    if ( a.eval )
        F = new MFMatrix(F->eval(a.z));

    if ( !a.decomposition.empty() )
    {
        set< int > D;
        for ( unsigned i = 0; i < a.decomposition.size(); ++i )
            for ( set< int >::const_iterator j = a.decomposition[i].begin(); j != a.decomposition[i].end(); ++j )
                if ( (*j) < 0 || F->dimension() <= (*j) || D.find(*j) != D.end() )
                {
                    ERROR("Invalid decomposition entry: " << ((*j) + 1) << ".");
                }
                else
                    D.insert(*j);
        
        if ( D.size() != (unsigned) F->dimension() )
            ERROR("Decomposition doesn't sum up to dimension: " << D.size() << " != " << F->dimension() << ".");
    }
    
    if ( a.range_type == "gbnr" )
    {
        if ( F->type() != "Matrix" )
            ERROR("Gershgorin discs are only possible for matrices.");

        for ( unsigned i = 0; i < a.gbnr_p.size(); ++i )
            if ( a.gbnr_p[i] <= 0 )
                ERROR("Gershgorin numbers must be positive.");

        if ( !a.gbnr_p.empty() && a.gbnr_p.size() != a.decomposition.size() )
            ERROR("Number of Gershgorin numbers must match decomposition size.");
    }

    if ( !a.J_matrix.empty() )
    {
        if ( J->dimension() != F->dimension() )
            ERROR("Dimension " << J->dimension() << " of J-matrix doesn't match dimension " << F->dimension());
    }
    
    if ( !a.bdsbnr_str.empty() && a.bounds_str.empty() && F->get_domain().empty() )
    {
        ERROR("bdsbnr needs bounds");
    }

    if ( a.ses && (F->type() != "Matrix" || a.range_type != "bnr" ) )
    {
        WARNING("--ses only for bnr of matrices.");
        a.ses = false;
    }

    if ( had_error )
        exit(1);

// PREPARE CALCULATION
// -------------------

    Ranges R(F, J);
    R.set_decomposition(a.decomposition);
    R.handle_signals = true;

// SHOW CONFIGURATION
// ------------------

    ofstream clog((output_filename + ".log").c_str(),ios::app);

#define OUT(X) { cout << X; clog << X; }
#define FOUT(X) { cout << setw(20) << left << X; clog << setw(20) << left << X; }

    FOUT("input file:" << a.matrix_function << endl);

    if ( !a.sub.empty() )
        FOUT("sub matrix:" << a.schur_str << endl);

    FOUT("range_type:" << a.range_type << endl);
    
    if ( a.range_type == "bds" )
    {
        FOUT("lambda:" << a.lambda << endl);
    }

    if ( !a.decomposition.empty() )
    {
        string d;
        FOUT("decomposition:" << a.decomposition_str << endl);
    }

    if ( !a.schur.empty() )
        FOUT("schur:" << a.schur_str << endl);

    FOUT("resolution:" << a.resolution  << endl);

    if ( !a.bounds_str.empty() )
        FOUT("bounds:" << a.bounds_str << endl);
    FOUT("png_size:" << a.png_width << " x " << a.png_height << endl);
    if ( a.xy_file )
        FOUT("Using xy_file:" << xy_filename << endl);
    if ( !e_xy.empty() )
        FOUT("Appending xy_file:" << xy_filename << endl);

    OUT("Matrix Function:\n");
    OUT((*(R.get_matrix_function())) << endl);


    if ( J != NULL )
    {
        OUT("J-matrix:");
        OUT(*J << "\n");
    }

// PERFORM CALCULATION
// -------------------


    if ( a.range_type == "info" )
    {
        R.info();
        return 0;
    }  


    vector< COMPLEX > e;
    vector< DOUBLE > g_rows, g_columns, g_ROWS, g_COLUMNS;

    stopper S;

    if ( a.range_type == "nr" )
    {
        e = R.bnr(a.resolution);
    }
    else if ( a.range_type == "bnr" )
    {
        if ( a.bdsbnr_str.empty() )
            e = R.bnr(a.resolution);
        else
            e = R.bdsbnr(a.xmin, a.xmax, a.bdsbnr_xres, 
                         a.ymin, a.ymax, a.bdsbnr_yres,
                         a.bdsbnr_eps, a.bdsbnr_max_iter);
    }
    else if ( a.range_type == "bds" )
    {
        e = R.bds(a.lambda, a.resolution);
    }
    else if ( a.range_type == "gbnr" )
    {
        e = R.gbnr(a.resolution, a.gbnr_p, g_rows, g_columns, g_ROWS, g_COLUMNS);
    }
    else if ( a.range_type == "ev" )
    {
        e = R.ev();
    }

    float user_seconds = S.user_seconds();

    cout << "Done.\n" << endl;

    if ( a.range_type == "ev" )
    {
        OUT("Eigenvalues:" << endl);
        for ( unsigned i = 0; i < e.size(); ++i )
            OUT("    " << e[i] << endl);
    }

    FOUT("# of points:" << e.size() << endl);
    FOUT("ev failures:" << "iter/ZERO/nan/info/ZF_EWC = "
                        << ComplexMatrix::ev_f_iter() << "/" 
                        << ComplexMatrix::ev_f_ZERO() << "/"
                        << ComplexMatrix::ev_f_nan()<< "/"
                        << ComplexMatrix::ev_f_info() << "/"
                        << floatZF::get_E_WRONG_COUNT() << endl
                        );
    FOUT("User seconds:" << user_seconds << endl);
    FOUT("Points/second:" << ((int)(e.size()/user_seconds)) << endl);


    vector< COMPLEX > *E = &e;
    
    if ( e.empty() )
        E = &e_xy;

    if ( a.range_type == "bds" )
    {
        E->push_back(0.0);
    }

    if ( !E->empty() && a.bounds_str.empty() )
    {
        FOUT("Bounds:");

        vector< DOUBLE > B = util::get_bounds(*E);

        DOUBLE XMIN = B[0], XMAX = B[1],
               YMIN = B[2], YMAX = B[3];
 
        XMIN -= .1*(XMAX - XMIN);
        XMAX += .1*(XMAX - XMIN);
        YMIN -= .1*(YMAX - YMIN);
        YMAX += .1*(YMAX - YMIN);

        if ( a.xmin > a.xmax )
        {
            a.xmin = XMIN;
            a.xmax = XMAX;
        }

        if ( a.ymin > a.ymax )
        {
            a.ymin = YMIN;
            a.ymax = YMAX;
        }

        OUT("[" << a.xmin << "," << a.xmax << "]x[" << a.ymin << "," << a.ymax << "]" << endl);
    }
    
    if ( a.range_type == "bds" )
    {
        E->erase(e.end());
    }


// WRITE PNG FILE
// --------------

    bool appended_to_png = false;

    if ( a.range_type == "gbnr" )
    {
        png_filename.push_back(output_filename + ".r.png");
        png_filename.push_back(output_filename + ".c.png");
        png_filename.push_back(output_filename + ".R.png");
        png_filename.push_back(output_filename + ".C.png");
    }
    else if ( a.ses )
    {
        for ( unsigned i = 0; i < a.decomposition.size(); ++i )
            png_filename.push_back(output_filename + ".C-" + util::to_string(i) + ".png");
    }
    
    else
    {
        png_filename.push_back(output_filename + ".png");
    }

    for ( unsigned i = 0; i < png_filename.size(); ++i )
    {
        fPlot *q = NULL;
        appended_to_png = false;
        if ( !a.no_append && !a.bounds_str.empty() ) 
            q = new fPlot(a.xmin,a.xmax,a.ymin,a.ymax,png_filename[i]);

        if ( q == NULL || !q->ready() )
        {
            delete q;
            q = new fPlot(a.xmin, a.xmax, a.ymin, a.ymax, 
                          a.png_width, a.png_height, true, true, a.grid);
        }
        else
        {
            appended_to_png = true;
            q->write((png_filename[i] + ".bck").c_str());
        }

        FOUT("Output file:" << png_filename[i] << (appended_to_png ? " (appended!)" : "" ) << endl);

        if ( !q->ready() )
            FATAL("Problem writing to " << png_filename[i]);

        P.push_back(*q);
    }

    p = P.begin();


    if ( !a.schur.empty() && !a.no_sss && !a.eval)
    {
        const Domain &s = ((MFSchurComplement*)F)->get_singularities();
        p->set_color(SSS_COLOR);
        for ( Domain::const_iterator i = s.begin(); i != s.end(); ++i )
        {
            p->filled_rectangle(i->xmin, i->ymax, i->xmax, i->ymin);
        }
    }


    char _r[] = " ", _g[] = " ", _b[] = " ";
    _r[0] = a.color[0];
    _g[0] = a.color[1];
    _b[0] = a.color[2];

    int r = (_r[0] == 'x' ? 255 : 16*strtol(_r, NULL, 16)),
        g = (_g[0] == 'x' ? 255 : 16*strtol(_g, NULL, 16)),
        b = (_b[0] == 'x' ? 255 : 16*strtol(_b, NULL, 16));
    
    for ( unsigned i = 0; i < P.size(); ++i )
        P[i].set_color(r, g, b);    

    if ( a.range_type == "gbnr" )
    {
        cout << "\nWriting points to png ..." << endl;
        plot_spheres(P[0], e, g_rows);
        plot_spheres(P[1], e, g_columns);
        
        unsigned i = 0;
        for ( unsigned h = 0; h < E->size(); ++h, ++i )
        {
            if ( i > a.decomposition.size() )
                i = 0;
            
            P[2].sphere(e[h].real(), e[h].imag(), g_ROWS[i], true);
            P[3].sphere(e[h].real(), e[h].imag(), g_COLUMNS[i], true);
        }
    }
    else
    {
        for ( unsigned s = 0; s < 2; ++s )
        {
            E = &e;
            if ( s == 1 )
            {
                E = &e_xy;
                if ( !E->empty() )
                    cout << "Appending points from " << xy_filename << " ... " << endl;
            }
            else
            {
                cout << "\nWriting new points to png ..." << endl;
            }
            for ( unsigned h = 0; h < E->size(); ++h )
            {
                float x = (*E)[h].real(),
                      y = (*E)[h].imag();

                if ( a.classical_colors )
                {
                    r = p->get_red(x, y);
                    g = p->get_green(x, y);
                    b = p->get_blue(x, y);

                    if ( r >= 1 && a.color[0] == 'x' )
                        r--;
                    else
                        r = 0;

                    if ( g >= 1 && a.color[1] == 'x' )
                        g--;
                    else
                        g = 0;

                    if ( b >= 1 && a.color[2] == 'x' )
                        b--;
                    else
                        b = 0;

                    p->set_color(r, g, b);
                }

                if ( a.range_type == "ev" )
                {
                    int size = (a.symbol_size >= 0 ? a.symbol_size : 5);
                    switch (a.symbol_style)
                    {
                        case 'c': p->circle(x, y, size); break;
                        case 'C': p->circle(x, y, size, true); break;
                        case 'b': p->box(x, y, size); break;
                        case 'B': p->box(x, y, size, true); break;
                        case 'x': p->cross(x, y, size); break;
                    }
                }
                else
                {
                    if ( a.xmin <= x && x <= a.xmax && a.ymin <= y && y <= a.ymax )
                        p->set(x, y);
                }
            
                if ( a.ses )
                {
                    p++;
                    if ( p == P.end() )
                        p = P.begin();
                }
            }
        }
    }
    
    if ( a.range_type == "bds" && a.bds_mark_zero )
    {
        char _r[] = " ", _g[] = " ", _b[] = " ";
        _r[0] = a.symbol_color[0];
        _g[0] = a.symbol_color[1];
        _b[0] = a.symbol_color[2];

        int r = (_r[0] == 'x' ? 255 : 16*strtol(_r, NULL, 16)),
            g = (_g[0] == 'x' ? 255 : 16*strtol(_g, NULL, 16)),
            b = (_b[0] == 'x' ? 255 : 16*strtol(_b, NULL, 16));

        p->set_color(r, g, b);
        int size = (a.symbol_size >= 0 ? a.symbol_size : 5);
        switch (a.symbol_style)
        {
            case 'c': p->circle(0.0, 0.0, size); break;
            case 'C': p->circle(0.0, 0.0, size, true); break;
            case 'b': p->box(0.0, 0.0, size); break;
            case 'B': p->box(0.0, 0.0, size, true); break;
            case 'x': p->cross(0.0, 0.0, size); break;
        }
    }

    for ( unsigned i = 0; i < P.size(); ++i )
        P[i].write(png_filename[i]);

    if ( a.xy_file && a.range_type != "gbnr" )
    {
        cout << "Writing new points to " << xy_filename << " ... " << endl;
        ofstream xy_out(xy_filename.c_str(), ios::binary | ios::app);
        float x[2];
        for ( unsigned i = 0; i < e.size(); ++i )
        {
            x[0] = e[i].real();
            x[1] = e[i].imag();
            xy_out.write((char*)&x, 2*sizeof(float));
        }
        xy_out.close();
    }

    FOUT("\n" << endl);

    return 0;
}
