// ComplexVector.hh

#ifndef __ComplexVector_hh
#define __ComplexVector_hh

#include <iostream>
#include <complex>
#include <vector>

#include <config.hh>

/// Very basic class representing complex vectors \f$v = (v_0,\dots,v_{n-1})\f$.
/**
\note
    - This is not a general purpose vector class; it only provides
      methods needed for the calculations of BNRs.
    - An \f$n\f$-dimensional vector is assumed to have the entries 
      \f$v_1,\dots,v_{n-1}\f$
    - 0-dimensional vectors are allowed
*/

class ComplexVector
{
   public:

        /// The empty vector
        ComplexVector();

        /// \p n dimensional vector filled with \p entry
        ComplexVector(const int &n, const COMPLEX *entry = NULL);

        /// Clone a vector
        ComplexVector(const ComplexVector &other);

        /// Convert a vector< COMPLEX > to ComplexVector
        ComplexVector(const std::vector< COMPLEX > &v);

        ~ComplexVector();

        /// Assignment
        const ComplexVector &operator = (const ComplexVector other);

/// @name Testing equality
//@{ 
        const bool operator == (const ComplexVector &other) const;
        const bool operator != (const ComplexVector &other) const;
//@}

        /// Get the dimension
        const int &dimension() const;

/// @name Testing for empty vectors
//@{ 
        const bool is_empty() const;
        const bool isnt_empty() const;
        const bool is_null() const;
        const bool isnt_null() const;
//@}

/// @name Access the entries
//@{
        /// Get \f$v_i\f$ (for assignment)
        COMPLEX &operator () (const int &i);
        /// Get \f$v_i\f$ (const version)
        const COMPLEX &operator () (const int &i) const;
//@}

/** @name Subvectors
    Get the subvector \f$(v_{i_1},\dots,v_{i_2})\f$ 
*/
//@{
        const ComplexVector sub(const int &k1, const int &k2);
        const ComplexVector operator () (const int &k1, const int &k2);
//@}

///@name Scalar multiplication
///@{ 
        const ComplexVector &operator *= (const COMPLEX &z);
        const ComplexVector  operator *  (const COMPLEX &z) const;
        const ComplexVector &operator /= (const COMPLEX &z);
        const ComplexVector  operator /  (const COMPLEX &z) const;
///@}

/// @name Functions
//@{
        /// Addition
        const ComplexVector &operator += (const ComplexVector &other);

        /// The scalar product with a vector
        const COMPLEX operator | (const ComplexVector &other) const;

        /// \f$\|x\|^2\f$
        const DOUBLE norm2() const;

        /// \f$\|x\|\f$
        const DOUBLE norm() const;
//@}

    protected:
        int dim;
        COMPLEX *entry;

        void entry_copy(COMPLEX *dest, const COMPLEX *src, const int &count);

        /// See Matrix.hh.
        void entry_init(const COMPLEX *entry = NULL);
};

std::ostream &operator << (std::ostream &out, const ComplexVector &v);
std::istream &operator >> (std::istream &in, ComplexVector &v);

#endif
