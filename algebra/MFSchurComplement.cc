// MFSchurComplement.cc

#include <cassert>
#include <cstdlib>
#include <MFSchurComplement.hh>
#include <Ranges.hh>
#include <ComplexMatrix.hh>

#define DEFAULT_SING_SIZE .1

using namespace std;

MFSchurComplement::MFSchurComplement(MatrixFunction *F, const set< int > &K)
{
    this->F = F;
    this->K = K;
    
    domain = F->get_domain();
    
    set< int > cK = ComplexMatrix(F->dimension()).complement(K);

    MatrixFunction *must_invert = F->sub_function(cK);
    vector< COMPLEX > e = must_invert->eigenvalues();
    delete must_invert;
    
    char *mfrange_sing_size = getenv("MFRANGE_SING_SIZE");
    
    float r = 0;
    if ( mfrange_sing_size != NULL )
        r = atof(mfrange_sing_size);
    if ( r <= 0 )
        r = DEFAULT_SING_SIZE;

    for ( unsigned i = 0; i < e.size(); ++i )
    {
        singularities.add(Box(e[i].real() - r, e[i].real() + r,
                              e[i].imag() - r, e[i].imag() + r));
    } 
    domain.remove(singularities);
}


const std::string MFSchurComplement::type() const
{
    return "Schur-complement";
}

const int MFSchurComplement::dimension() const 
{
    return K.size();
}

const ComplexMatrix MFSchurComplement::eval(const COMPLEX &z)
{
    return F->eval(z).schur_complement(K);
}

MatrixFunction *MFSchurComplement::sub_function(const std::set< int > &K)
{
    ASSERT(0,"DON'T USE ME THIS WAY.");
}

const Domain &MFSchurComplement::get_singularities() const
{
    return singularities;
}

const set< int > &MFSchurComplement::get_K() const
{
    return K;
}

const MatrixFunction *MFSchurComplement::get_F() const
{
    return F;
}
