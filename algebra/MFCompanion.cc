// MFCompanion.cc

#include <cassert>
#include <cstdlib>
#include <MFCompanion.hh>
#include <Ranges.hh>
#include <ComplexMatrix.hh>

#define DEFAULT_SING_SIZE .1

using namespace std;

MFCompanion::MFCompanion(const MatrixPolynomial &P)
: MFPolynomial(P) // dummy
{
    ASSERT(P.isnt_empty(), "");
    ASSERT(P[0].isnt_empty(), "");
    
    deg = 1;
    const int &degP = P.degree(),
              &dimP = P.dimension();

    int dim = dimP*degP;

    delete[] C;
    C = new ComplexMatrix[2];
    C[0] = ComplexMatrix(dim);
    C[1] = ComplexMatrix(dim);

    for ( int i = 0; i < dim - dimP; ++i )
    {
        C[0](i, i + degP) = -1;
        C[1](i, i) = 1;
    }

    for ( int i = 0; i < dimP; ++i )
        for ( int j = 0; j < degP; ++j )
            for ( int k = 0; k < dimP; ++k )
                C[0](dim - dimP + i,j*dimP + k) = P[j](i,k);
    
    for ( int i = 0; i < dimP; ++i )
        for ( int j = 0; j < dimP; ++j )
            C[1](dim - dimP + i,dim - dimP + j) = P[degP](i,j);
}

const std::string MFCompanion::type() const
{
    return "Companion";
}

