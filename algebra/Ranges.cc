// Ranges.cc

#include <cassert>
#include <fstream>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

#include <Ranges.hh>
#include <ComplexMatrix.hh>
#include <MatrixPolynomial.hh>
#include <MFParser.hh>
#include <genrand.h>

using namespace std;

Ranges::Ranges()
{
    F = NULL;
    J = NULL;
    _init();
}

Ranges::Ranges(MatrixFunction *F, ComplexMatrix *J)
{
    ASSERT( F != NULL, "");
    if ( J != NULL )
        ASSERT(F->dimension() == J->dimension(), SHOW_VAR(*F)AND_SHOW_VAR(*J));

    this->F = F;
    this->J = J;
    _init();
}

Ranges::~Ranges()
{
//     if ( F != NULL )
//         delete F;
}

void Ranges::set_decomposition(const std::vector< std::set< int > > decomposition)
{
    this->decomposition = decomposition;
}

void Ranges::set_matrix_function(MatrixFunction *F)
{
    ASSERT(F != NULL, "");
    this->F = F;
}

void Ranges::set_J(ComplexMatrix *J)
{
    if ( J != NULL )
        ASSERT(F->dimension() == J->dimension(), SHOW_VAR(*F)AND_SHOW_VAR(*J));    

    this->J = J;
}

MatrixFunction *Ranges::get_matrix_function()
{
    return F;
}

vector< COMPLEX > Ranges::bnr(const int &res)
{
    assert ( F != NULL );
    vector< COMPLEX > r;
    vector< COMPLEX > one;

    enable_signal_handler();
    for ( int i = 0; i < res; ++i )
    {
        vector< ComplexVector > x = random_block_normed();
        
// cout << "x[0] = " << x[0] << ", ||x[0]|| = " << x[0].norm() << endl;
// cout << "_J(x)[0] = " << _J(x)[0] << ", ||_J(x)[0]|| = " << _J(x)[0].norm() << endl;
// cout << 
// cout << "block_sp = " << F->eval(0).block_sp(decomposition, x, _J(x)) << endl;

        one = F->block_eigenvalues(decomposition, x, _J(x));
        r.insert(r.end(), one.begin(), one.end());
        if ( leave || print_state )
            show_state(i, res);
        if ( leave )
            break;
    }
    disable_signal_handler();
    return r;
}

vector< COMPLEX > Ranges::bds(const COMPLEX &z, const int &res)
{
    assert ( F != NULL );
    vector< COMPLEX > r;

    enable_signal_handler();
    for ( int i = 0; i < res; ++i )
    {
        vector< ComplexVector > x = random_block_normed();

        r.push_back(F->eval(z).block_sp(decomposition, x, _J(x)).det());
        if ( leave || print_state )
            show_state(i, res);
        if ( leave )
            break;
    }
    disable_signal_handler();

    return r;
}

vector< COMPLEX > Ranges::ev()
{
    assert ( F != NULL );
    return F->eigenvalues();
}

vector< COMPLEX > Ranges::gbnr(const int &res, const vector<DOUBLE> &p, 
                               vector<DOUBLE> &r, vector<DOUBLE> &c,
                               vector<DOUBLE> &R, vector<DOUBLE> &C
                               )
{
    assert ( F != NULL && F->type() == "Matrix" );
    ComplexMatrix A = F->eval(0), B;
    int n = decomposition.size();
    vector<COMPLEX> d;

    vector<DOUBLE> P = p;
    if ( P.empty() )
        for ( int i = 0; i < n; ++i )
            P.push_back(1.0);

    DOUBLE _R, _C;
    R.clear();
    C.clear();
    for ( int i = 0; i < n; ++i )
    {
        _R = 0;
        for ( int j = 0; j < n; ++j )
            if ( i != j )
                _R += P[j]*((ComplexMatrix)A.sub(decomposition[i],decomposition[j])).norm();
        _R /= P[i];

        _C = 0;
        for ( int j = 0; j < n; ++j )
            if ( i != j )
                _C += ((ComplexMatrix)A.sub(decomposition[j],decomposition[i])).norm()/P[j];
        _C *= P[i];

        R.push_back(_R);
        C.push_back(_C);
    }

    DOUBLE _r, _c;
    r.clear();
    c.clear();
    enable_signal_handler();
    for ( int h = 0; h < res; ++h )
    {
        vector< ComplexVector > x = random_block_normed();
        B = A.block_sp(decomposition, x, _J(x));
        
        for ( int i = 0; i < n; ++i )
        {
            _r = 0;
            for ( int j = 0; j < n; ++j )
                if ( i != j )
                    _r += P[j]*abs(B(i,j));
            _r /= P[i];

            _c = 0;
            for ( int j = 0; j < n; ++j )
                if ( i != j )
                    _c += abs(B(j,i))/P[j];
            _c *= P[i];

            d.push_back(B(i,i));
            r.push_back(_r);
            c.push_back(_c);
        }
        if ( leave || print_state )
            show_state(h, res);
        if ( leave )
            break;
    }
    disable_signal_handler();
    return d;
}


vector< COMPLEX > Ranges::bdsbnr(const DOUBLE &xmin, const DOUBLE &xmax, const int &xres,
                                 const DOUBLE &ymin, const DOUBLE &ymax, const int &yres,
                                 const DOUBLE &eps, const int &max_iter)
{
    const int version = 1;

    assert ( F != NULL );
    vector< COMPLEX > r;

    vector< vector< ComplexVector > > N;
    for ( int h = 0; h < max_iter; ++h )
        N.push_back(random_block_normed());

    enable_signal_handler();

    DOUBLE dx = (xmax - xmin)/xres,
           dy = (ymax - ymin)/yres;

    DOUBLE x = xmin, 
           y = ymin;
    for ( int i = 0; i < xres && !leave; ++i, x += dx, y = ymin )
    {
        for ( int j = 0; j < yres && !leave; ++j, y += dy )
        {
            COMPLEX z(x,y);
            if ( !F->get_domain().empty() && !F->get_domain().contains(z) )
                continue;

            ComplexMatrix A = F->eval(z);

            // Check if $0 \in D_\H(A)$:

            vector< DOUBLE > args;
            // args.push_back(-3.141592654);
            args.push_back(-1.570796327);
            args.push_back(0);
            args.push_back(1.570796327);
            args.push_back(3.141592654);

            if ( version == 1 )
            {
                for ( int h = 0; h < max_iter; ++h )
                {
                    vector< ComplexVector > x = N[h];
                    if ( abs(A.block_sp(decomposition, x, _J(x)).det()) < eps )
                    {
                        r.push_back(z);
                        break;
                    }

                    if ( leave || print_state )
                        show_state(i*yres + j, xres*yres);
                }
            }
            else if ( version == 2 )
            {
                vector< bool > have_arg;
                for ( unsigned k = 0; k < args.size(); ++k )
                    have_arg.push_back(false);

                for ( int h = 0; h < max_iter && !leave; ++h )
                {
                    vector< ComplexVector > x = N[h];
                    COMPLEX d = A.block_sp(decomposition, x, _J(x)).det();
                    
                    if ( abs(d) < eps )
                    {
                        r.push_back(z);
                        break;
                    }

                    DOUBLE a = arg(d);
                    bool have_args = true;
                    for ( unsigned k = 0; k < args.size(); ++k )
                    {
                        if ( abs(a - args[k]) < eps )
                            have_arg[k] = true;
                        
                        if ( !have_arg[k] )
                            have_args = false;
                    }

                    if ( have_args )
                    {
                        r.push_back(z);
                        break;
                    }

                    if ( leave || print_state )
                        show_state(i*yres + j, xres*yres);
                }
            }
            else // Version 3: combination of versions 1 and 2
            {
                vector< bool > have_arg;
                for ( unsigned k = 0; k < args.size(); ++k )
                    have_arg.push_back(false);

                for ( int h = 0; h < max_iter && !leave; ++h )
                {
                    vector< ComplexVector > x = N[h];
                    DOUBLE a = arg(A.block_sp(decomposition, x, _J(x)).det());
                    for ( unsigned k = 0; k < args.size(); ++k )
                    {
                        if ( abs(a - args[k]) < eps )
                            have_arg[k] = true;
                    }

                    bool have_args = true;
                    for ( unsigned k = 0; k < args.size(); ++k )
                        if ( !have_arg[k] )
                            have_args = false;

                    if ( have_args )
                    {
                        r.push_back(z);
                        break;
                    }

                    if ( leave || print_state )
                        show_state(i*yres + j, xres*yres);
                }
            }
        }
    }
    disable_signal_handler();

    return r;
}


void Ranges::_init()
{
    handle_signals = false;
    srand(time(0));
    sgenrand(time(0));
}

vector< ComplexVector > Ranges::random_block_normed()
{
    vector< ComplexVector > x;

    if ( decomposition.empty() )
    {
        x.push_back(random_normed(F->dimension()));
        return x;
    }

    for ( unsigned k = 0; k < decomposition.size(); ++k )
    {
//  Formerly:
//      ComplexVector y = random_normed(decomposition[k].size());
        ComplexVector y = random_normed(k);
        x.push_back(y);
    }

    return x;
}

vector< ComplexVector > Ranges::_J(const vector< ComplexVector > &x)
{
    vector< ComplexVector > y;
    if ( J != NULL )
    {
        for ( unsigned i = 0; i < x.size(); ++i )
            y.push_back(((ComplexMatrix)J->sub(decomposition[i],decomposition[i]))*x[i]);
    }
    else
        y = x;
    
    return y;
}


ComplexVector Ranges::random_normed(const int &k)
{
    int dim;
    if ( decomposition.empty() )
        dim = F->dimension();
    else        
        dim = decomposition[k].size();

    ComplexVector v(dim);
    DOUBLE norm;

    if ( J != NULL )
    {
        ComplexMatrix J_k(dim);
        
        if ( decomposition.empty() )
            J_k = *J;
        else
            J_k = (ComplexMatrix)J->sub(decomposition[k],decomposition[k]);
        do
        {
            for ( int j = 0; j < dim; ++j ) 
            {
                v(j) = polar ( DOUBLE(genrand()), DOUBLE(genrand() * 2 * PI ) );
            }
            norm = sqrt((v|J_k*v).real());
        } while ( norm == 0 );
    }
    else
    {
        do
        {
            for ( int j = 0; j < dim; ++j ) 
            {
                v(j) = polar ( DOUBLE(genrand()), DOUBLE(genrand() * 2 * PI ) );
            }
            norm = v.norm();
        } while ( norm == 0 );
    }

    return v/norm;
}


// static 
void Ranges::enable_signal_handler(const bool &initial)
{
    if ( !handle_signals )
        return;

    if ( initial )
    {
        leave = print_state = false;
        cerr << "Installing signal handler (my pid: " << getpid() << ")." << endl;
    }

    if ( signal(SIGINT, Ranges::signal_handler ) == SIG_ERR )
    {
        cerr << "Ranges::sig_handler() (SIGINT) couldn't be initialised."
             << "Will not stop calculation on Ctrl-C." << endl;
    }
    if ( signal(SIGTERM, Ranges::signal_handler ) == SIG_ERR )
    {
        cerr << "Ranges::sig_handler() (SIGTERM) couldn't be initialised."
             << "Will not abort cleanly." << endl;
    }
    if ( signal(SIGHUP, Ranges::signal_handler ) == SIG_ERR )
    {
        cerr << "Ranges::sig_handler() (SIGHUP) couldn't be initialised."
             << "Will not abort cleanly." << endl;
    }
    if ( signal(SIGUSR1, Ranges::signal_handler ) == SIG_ERR )
    {
        cerr << "Ranges::sig_handler() (SIGUSR1) couldn't be initialised."
             << "Will not react." << endl;
    }
}

// static 
void Ranges::disable_signal_handler()
{
    if ( !handle_signals )
        return;

    signal(SIGINT, SIG_DFL);
    signal(SIGTERM, SIG_DFL);
    signal(SIGHUP, SIG_DFL);
    signal(SIGUSR1, SIG_IGN);
}

// static
void Ranges::signal_handler(int sig)
{
    signal(SIGINT, SIG_IGN);
    signal(SIGTERM, SIG_IGN);
    signal(SIGHUP, SIG_IGN);
    signal(SIGUSR1, SIG_IGN);

    if ( sig == SIGUSR1 )
    {
        print_state = true;
        enable_signal_handler(false);
    }
    else
    {
        cerr << "Catched signal #" << sig << ". Trying to leave cleanly." << endl;
        leave = true;
    }
}

void Ranges::show_state(const int &i, const int &res, ostream &out)
{
    cout << i << "/" << res << " = " << (int)(float(i)/res*100) << "%" << endl;
    print_state = false;
}

bool Ranges::handle_signals = false;
bool Ranges::leave = false;
bool Ranges::print_state = false;

