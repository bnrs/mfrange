all: libalgebra libnetlib libutil mfrange

libalgebra:
	cd algebra && make

libnetlib:
	cd algebra/netlib && make
    
libutil:
	cd util && make

mfrange:
	cd cl && make

doc:
	doxygen Doxyfile
    
clean:
	cd algebra && make clean
	cd algebra/netlib && make clean
	cd util && make clean
	cd cl && make clean    
