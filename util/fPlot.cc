#include <fPlot.hh>

#include <iostream>
#include <fstream>
#include <cmath>
#include <cassert>
#include <string>
#include <cstdio>
#include <vector>
#include <png.h>
#include <gd.h>
#include <gdfontl.h>
#include <util.hh>

using namespace std;

fPlot::fPlot(const float &_xmin, const float &_xmax, 
             const float &_ymin, const float &_ymax,
             const unsigned &_width, const unsigned &_height,
             const bool &_xnumbers, const bool &_ynumbers,
             const bool &_draw_grid)
{
    xmin = -1; xmax = 1; 
    if ( _xmin < _xmax )
    {
        xmin = _xmin;
        xmax = _xmax;
    }
    ymin = -1; ymax = 1;
    if ( _ymin < _ymax )
    {
        ymin = _ymin;
        ymax = _ymax;
    }
    dx = xmax - xmin;
    dy = ymax - ymin;

    width = DEFAULT_WIDTH;
    if ( _width > 0 )
        width = _width;
    height = DEFAULT_HEIGHT;
    if ( _height > 0 )
        height = _height;

    left = BORDER_LEFT;
    top = BORDER_TOP;
    right = width - BORDER_RIGHT;
    bottom = height - BORDER_BOTTOM;
    DH = right - left;
    DV = bottom - top;

    image = gdImageCreateTrueColor(width, height);

    white = allocate_color(255, 255, 255); 
        //  First color allocated gives the background!
        //  Uuups, seems not to work for true color images anymore...
        //  -> Draw a white filled rectangle in draw_coordinates!
    black = allocate_color(0, 0, 0); 
    lightgray = allocate_color(220, 220, 220); 
    green = allocate_color(0, 255, 0); 

    draw_grid = _draw_grid;
    xnumbers = _xnumbers;
    ynumbers = _ynumbers;
    draw_coordinates();
}

fPlot::fPlot(const fPlot &other)
{
    width = other.width;
    height = other.height;
    left = other.left;
    right = other.right;
    top = other.top;
    bottom = other.bottom;
    DH = other.DH;
    DV = other.DV;
    current_color = other.current_color;
    xmin = other.xmin;
    xmax = other.xmax;
    ymin = other.ymin;
    ymax = other.ymax;
    dx = other.dx;
    dy = other.dy;
    draw_grid = other.draw_grid;
    xnumbers = other.xnumbers;
    ynumbers = other.ynumbers;
    black = other.black;
    white = other.white;
    lightgray = other.lightgray;
    green = other.green;
    
    image = gdImageCreateTrueColor(width, height);
    gdImageCopy(image, other.image, 0, 0, 0, 0, width, height);
}


fPlot::fPlot(const float &_xmin, const float &_xmax, 
             const float &_ymin, const float &_ymax,
             const string &filename)
{
    xmin = -1; xmax = 1; 
    if ( _xmin < _xmax )
    {
        xmin = _xmin;
        xmax = _xmax;
    }
    ymin = -1; ymax = 1;
    if ( _ymin < _ymax )
    {
        ymin = _ymin;
        ymax = _ymax;
    }
    dx = xmax - xmin;
    dy = ymax - ymin;

    FILE *image_in = NULL;
    image_in = fopen(filename.c_str(), "rb");

    if ( image_in == NULL )
    {
        image = NULL;
    }
    else
    {
        image = gdImageCreateFromPng(image_in);
        fclose(image_in);
    }

    if ( image == NULL )
        return;
    
    width = image->sx;
    height = image->sy;

    left = BORDER_LEFT;
    top = BORDER_TOP;
    right = width - BORDER_RIGHT;
    bottom = height - BORDER_BOTTOM;
    DH = right - left;
    DV = bottom - top;

    white = allocate_color(255, 255, 255); 
    black = allocate_color(0, 0, 0); 
    lightgray = allocate_color(220, 220, 220); 
    green = allocate_color(0, 255, 0); 
}

fPlot::~fPlot()
{ 
    if ( image != NULL )
        gdImageDestroy(image);
}

const bool fPlot::ready() const
{
    return image != NULL;
}

const bool fPlot::write(const string &filename)
{
    if ( !ready() )
        return false;
    
    FILE *image_out = fopen(filename.c_str(), "wb");
    if ( image_out == NULL )
    {
        cerr << "Error opening " << filename << " for opening!" << endl;
        return false;
    }
    else
    {
        gdImagePng(image, image_out);

        fclose(image_out);
        return true;
    }
}

const float &fPlot::get_xmin() const
{
    return xmin;
}

const float &fPlot::get_xmax() const
{
    return xmax;
}

const float &fPlot::get_ymin() const
{
    return ymin;
}

const float &fPlot::get_ymax() const
{
    return ymax;
}

const int fPlot::getiX(const float &x) const
{
    return left   + (int)((x - xmin)*DH/dx);
}

const int fPlot::getiY(const float &y) const
{
    return bottom - (int)((y - ymin)*DV/dy);
}

const float fPlot::getfX(const int &ix) const
{
    return xmin + (ix - left)*dx/DH;
}

const float fPlot::getfY(const int &iy) const
{
    return -(ymin + (iy - bottom)*dy/DV);
}

int fPlot::get_red(const float &x, const float &y ) const
{
    return gdImageRed(image, gdImageGetPixel(image, getiX(x), getiY(y)));
}

int fPlot::get_green(const float &x, const float &y ) const
{
    return gdImageGreen(image, gdImageGetPixel(image, getiX(x), getiY(y)));
}

int fPlot::get_blue(const float &x, const float &y ) const
{
    return gdImageBlue(image, gdImageGetPixel(image, getiX(x), getiY(y)));
}

void fPlot::set_color(const int &red, const int &green, const int &blue)
{
    current_color = allocate_color(red, green, blue);
}

void fPlot::set(float x, float y)
{
    if ( !ready() )
        return;

    gdImageSetPixel(image, getiX(x), getiY(y), current_color);
}

void fPlot::set(const complex<float> &z)
{
    set(z.real(), z.imag());
}

void fPlot::set(const vector< complex<float> > &z)
{
    for ( vector< complex<float> >::const_iterator i = z.begin(); i != z.end(); i++ )
        set(*i);
}

void fPlot::line(const float &x1, const float &y1, const float &x2, const float &y2)
{
    if ( !ready() )
        return;

    gdImageLine(image, getiX(x1), getiY(y1), getiX(x2), getiY(y2), current_color);
}

void fPlot::line(const complex<float> &z1, const complex<float> &z2)
{
    line(z1.real(), z1.imag(), z2.real(), z2.imag());
}

void fPlot::rectangle(const float &x1, const float &y1, const float &x2, const float &y2)
{
    if ( !ready() )
        return;

    gdImageRectangle(image, getiX(x1), getiY(y1), getiX(x2), getiY(y2), current_color);
}

void fPlot::rectangle(const complex<float> &z1, const complex<float> &z2)
{
    rectangle(z1.real(), z2.real(), z1.imag(), z2.imag());
}

void fPlot::filled_rectangle(const float &x1, const float &y1, const float &x2, const float &y2)
{
    if ( !ready() )
        return;

    gdImageFilledRectangle(image, getiX(x1), getiY(y1), getiX(x2), getiY(y2), current_color);
}

void fPlot::filled_rectangle(const complex<float> &z1, const complex<float> &z2)
{
    filled_rectangle(z1.real(), z2.real(), z1.imag(), z2.imag());
}

void fPlot::circle (const float &x, const float &y, const int &radius, const bool &solid)
{ 
    if ( !ready() )
        return;

    if ( solid )
        gdImageFilledArc(image, getiX(x), getiY(y), 2*radius, 2*radius, 0, 360, current_color, gdArc);
    else
        gdImageArc(image, getiX(x), getiY(y), 2*radius, 2*radius, 0, 360, current_color);
}

void fPlot::circle(const complex<float> &z, const int &radius, const bool &solid)
{
    circle(z.real(), z.imag(), radius, solid);
}

void fPlot::circle(const vector< complex<float> > &z, const int &radius, const bool &solid)
{
    for ( vector< complex<float> >::const_iterator i = z.begin(); i != z.end(); i++ )
        circle(*i, radius, solid);
}

void fPlot::box (const float &x, const float &y, const int &size, const bool &solid)
{ 
    if ( !ready() )
        return;

    int iX = getiX(x),
        iY = getiY(y);

    if ( solid )
        gdImageFilledRectangle(image, iX-size, iY-size, iX+size, iY+size, current_color);
    else
        gdImageRectangle(image, iX-size, iY-size, iX+size, iY+size, current_color);
}

void fPlot::box(const complex<float> &z, const int &size, const bool &solid)
{
    box(z.real(), z.imag(), size, solid);
}

void fPlot::box(const vector< complex<float> > &z, const int &size, const bool &solid)
{
    for ( vector< complex<float> >::const_iterator i = z.begin(); i != z.end(); i++ )
        box(*i, size, solid);
}

void fPlot::cross (const float &x, const float &y, const int &size)
{ 
    if ( !ready() )
        return;

    int iX = getiX(x),
        iY = getiY(y);

    gdImageLine(image, iX-size, iY+size, iX+size, iY-size, current_color);
    gdImageLine(image, iX+size, iY+size, iX-size, iY-size, current_color);
}

void fPlot::cross(const complex<float> &z, const int &size)
{
    cross(z.real(), z.imag(), size);
}

void fPlot::cross(const vector< complex<float> > &z, const int &size)
{
    for ( vector< complex<float> >::const_iterator i = z.begin(); i != z.end(); i++ )
        cross(*i, size);
}

void fPlot::sphere (const float &x, const float &y, const float &radius, const bool &solid)
{ 
    if ( !ready() )
        return;

    int h_diam = int(2*radius*DH/dx),
        v_diam = int(2*radius*DV/dy);

    set(x, y);

    if ( solid )
        gdImageFilledArc(image, getiX(x), getiY(y), h_diam, v_diam, 0, 360, current_color, gdArc);
    else
        gdImageArc(image, getiX(x), getiY(y), h_diam, v_diam, 0, 360, current_color);
}

void fPlot::sphere(const complex<float> &z, const float &radius, const bool &solid)
{
    sphere(z.real(), z.imag(), radius, solid);
}

void fPlot::polygon(const vector<float> &x, const vector<float> &y)
{
    unsigned length = x.size();
    if ( length != y.size() )
    {
        cerr << "fPlot.polygon(): Got differently sized x and y coordinates!" << endl;
        return;
    }

    vector< complex<float> > z(length);
    for ( unsigned i = 0; i < length; i++ )
        z[i] = complex<float>(x[i], y[i]);
    polygon(z);
}

void fPlot::polygon(const vector< complex<float> > &z)
{
    if ( !ready() )
        return;

    int length = z.size();
    gdPoint *points = new gdPoint[length];
    for ( int i = 0; i < length; i++ )
    {
        points[i].x = getiX(z[i].real());
        points[i].y = getiX(z[i].imag());
    }
    gdImagePolygon(image, points, length, current_color);
    delete[] points;
}

void fPlot::draw_coordinates()
{
    if ( !ready() )
        return;

    gdImageFilledRectangle(image, 0, 0, width, height, white);
    gdImageRectangle(image, left, top, right, bottom, black);

    int   ox = entier(log10(dx));
    float sx = pow(10.0,ox);

    if ( xnumbers )
    {
        if ( log10(dx) - ox > 0.8 ) sx *= 2;
        else if ( log10(dx) - ox < 0.6 ) sx /= 2;
        float tx = entier(xmin, ox);
        int ix = getiX(tx);
        while ( ix <= right )
        {
            if ( ix >= left )
            {
                gdImageLine(image, ix, bottom, ix, bottom + 5, black);
                if ( draw_grid && ix != left && ix != right )
                    gdImageLine(image, ix, top+1, ix, bottom-1, lightgray);
                string X = util::to_string(tx);
                int iX = ix - X.size()*gdFontLarge->w/2;
                gdImageString(image, gdFontLarge, iX, bottom+10, 
                              (unsigned char *)X.c_str(), black);
            }
            tx += sx;
            ix = getiX(tx);
        }
    }
    
    if ( ynumbers )
    {
        int   oy = entier(log10(dy));
        float sy = pow(10.0,oy);
        if ( log10(dy) - oy > 0.8 ) sy *= 2;
        else if ( log10(dy) - oy < 0.6 ) sy /= 2;
        float ty = entier(ymin, oy);
        int iy = getiY(ty);
        while ( iy >= top )
        {
            if ( iy <= bottom )
            {
                gdImageLine(image, left - 5, iy, left, iy, black);
                if ( draw_grid && iy != top && iy != bottom )
                    gdImageLine(image, left+1, iy, right-1, iy, lightgray);
                string Y = util::to_string(ty);
                int iY = iy + Y.size()*gdFontLarge->w/2;
                gdImageStringUp(image, gdFontLarge, left-25, iY, 
                              (unsigned char *)Y.c_str(), black);
            }
            ty += sy;
            iy = getiY(ty);
        }
    }
}

const int fPlot::allocate_color(const int &red, const int &green, const int &blue) const
{
    return gdTrueColor(red, green, blue);
}

const int fPlot::entier(const float &x) const
{
    return (int)floor(x);
}

const float fPlot::entier(const float &x, const int &ord) const
{
    return (float)pow(10.0,ord)*entier(x*(float)pow(10.0,-ord));
}

const int fPlot::ceil(const float &x) const
{
//  *** FIX ME !!! //
//  Doesn't work for SunOS!
    return (int)ceil(x);
}

