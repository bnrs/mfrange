// mfrange_args.cc

#include <strstream>
#include <cassert>
#include <cstdlib>
#include <tclap/CmdLine.h>
#include <util.hh>
#include <floatZF.hh>
#include "mfrange_args.hh"

using namespace std;
using namespace TCLAP;

mfrange_args::mfrange_args()
{
    reset();
}

mfrange_args::mfrange_args(int argc, char *argv[])
{
    reset();
    parse_args(argc, argv);
}

void mfrange_args::reset()
{
    xmin = 1; xmax = 0;
    ymin = 1; ymax = 0;
    png_width = png_height = 0;
    resolution = 0;
    grid = false;
    eval = false;
    xy_file = false;
}

void mfrange_args::parse_args(int argc, char *argv[])
{
    try {
        CmdLine cmd(
"ENVIRONMENT VARIABLES:\n\
MFRANGE_ZF_CONFIG max_rect:line_length:precision:subdivisions<:debug>\n\
MFRANGE_SING_SIZE size"
, ' ', "");

        vector<string> hide; hide.push_back("ignore_rest"); hide.push_back("version");
        ((StdOutput *)cmd.getOutput())->hide_in_help = hide;

// --nr
        SwitchArg _nr(
            "", "nr", 
            "Numerical range",
            false);

// --bnr
        SwitchArg _bnr(
            "", "bnr", 
            "Block numerical range",
            false);

// --ev
        SwitchArg _ev(
            "", "ev", 
            "Eigenvalues", false);

// --bds
        ValueArg<COMPLEX> _bds(
            "", "bds", 
            "Block determinant set with respect to x + iy", 
            false, COMPLEX(0.0), "\'(x,y)\'");

// --info
        SwitchArg _info(
            "", "info", 
            "Display some sort of information.",
            false);


        vector<Arg *> action;
        action.push_back(&_nr);
        action.push_back(&_bnr);
        action.push_back(&_ev);
        action.push_back(&_bds);
        action.push_back(&_info);

#ifdef MK_EXP
// --gbnr
        SwitchArg _gbnr(
            "", "gbnr", 
            "'Gershgorin' block numerical range",
            false);

        action.push_back(&_gbnr);
#endif


        cmd.xorAdd(action);

// -m, --matrix_function
        ValueArg<string> _matrix_function(
            "m", "matrix_function",
            "The file containing the matrix function",
            true, "", "file name", cmd);

// --sub
        ValueArg<string> _sub(
            "", "sub",
            "Apply to a submatrix of the matrix function.",
            false, "", "k1,k2,...", cmd);

// --companion
        SwitchArg _companion(
            "", "companion",
            "Apply to the companion polynomial of a matrix polynomial.",
            false, cmd);

// --eval
        ValueArg<COMPLEX> _eval(
            "", "eval", 
            "Apply to F(z)", 
            false, COMPLEX(0.0), "\'(x,y)\'", cmd);

// --no_sss
        SwitchArg _no_sss(
            "", "no_sss", 
            "Don't show singularities for Schur complement.",
            false, cmd);

// -r, --resolution
        ValueArg<unsigned> _resolution(
            "r", "resolution",
            "The output resolution.",
            false, 0, "resolution", cmd);

// -d, --decomposition_str
        ValueArg<string> _decomposition_str(
            "d", "decomposition",
            "The decomposition in one of the forms k11,12,..:k21,k22,...:kn1,kn2 or d1x...xdn",
            false, "", "[k11,12,..:k21,k22,...:kn1,kn2|d1x...xdn", cmd);

// -b, --bounds
        ValueArg<string> _bounds(
            "b", "bounds",
            "The bounds. If e.g. only the range in y-direction is to be given then set xmin = xmax.",
            false, "", "xmin:xmax:ymin:ymax", cmd);

// --grid
        SwitchArg _grid(
            "", "grid", 
            "Plot grid",
            false, cmd);

// -S, --png_size
        ValueArg<string> _png_size(
            "S", "png_size",
            "The dimension of the png file",
            false, "", "width:height", cmd);

// -c, --color

        ValueArg<string> _color(
            "c", "color",
            "Color to use in the plot (r,g,b are each one of 0 or x)",
            false, "x00", "rgb", cmd);

// --symbol_color

        ValueArg<string> _symbol_color(
            "", "symbol_color",
            "Color to use for symbols in the plot (r,g,b are each one of 0 or x)",
            false, "x00", "rgb", cmd);

// -C, --classical_colors
        SwitchArg _classical_colors(
            "C", "classical_colors",
            "Plot using the classical BNR style",
            false, cmd);

// --symbol_style
        ValueArg<char> _symbol_style(
            "", "symbol_style",
            "The symbol used if plotting eigenvalues: \
circle (c, solid: C), box (b, solid: B), or cross (x).",
            false, 'c', "<c|C|b|B|x>", cmd);

// --symbol_size
        ValueArg<int> _symbol_size(
            "", "symbol_size",
            "The size of the symbol used if plotting eigenvalues.",
            false, 5, "size", cmd);

// --no_append
        SwitchArg _no_append(
            "", "no_append",
            "Always create a new png file. Don't 'append' to an existing one.",
            false, cmd);

// --xy_file
        SwitchArg _xy_file(
            "", "xy_file", "\
Additionally write a (binary) file containing the calculated points. \
The points in this file will subsequently be used for the creation of \
png files.",
            false, cmd);

// --bds_mark_zero
        SwitchArg _bds_mark_zero(
            "", "bds_mark_zero", 
            "Mark 0 if plotting a bds.",
            false, cmd);

// --ses
        SwitchArg _ses(
            "", "ses", 
            "'Store eigenvalues in separate files' (...)",
            false, cmd);

// --zf_config
        ValueArg<string> _zf_config(
            "", "zf_config",
            "",
            false, "", "max_rect:line_length:precision:subdivisions<:debug>", cmd);


#ifdef MK_EXP
// --schur
        ValueArg<string> _schur(
            "", "schur",
"Apply to the Schur complement of the matrix function. \
(See environment variable MFRANGE_SING_SIZE.)",
            false, "", "k1,k2,...", cmd);

// -J, --J
        ValueArg<string> _J_matrix(
            "J", "J_matrix",
            "The file containing the J-matrix",
            false, "", "file name", cmd);

// --bdsbnr
        ValueArg<string> _bdsbnr(
            "", "bdsbnr",
            "...",
            false, "", "xres:yres:eps:max_iter", cmd);

// --gbnr_p
        ValueArg<string> _gbnr_p(
            "", "gbnr_p",
            "The positive numbers p_1,...,p_n used in Gershgorin's Theorem.",
            false, "", "p1,...,pn", cmd);
#endif

        cmd.parse(argc, argv);

        matrix_function        = _matrix_function.getValue();
        sub_str                = _sub.getValue();
        companion              = _companion.getValue();
        resolution             = _resolution.getValue(); 
        decomposition_str      = _decomposition_str.getValue();
        bounds_str             = _bounds.getValue();
        grid                   = _grid.getValue();
        png_size_str           = _png_size.getValue();
        color                  = _color.getValue();
        symbol_color           = (_symbol_color.isSet() ? _symbol_color.getValue() : color);
        classical_colors       = _classical_colors.getValue();
        no_append              = _no_append.getValue();
        no_sss                 = _no_sss.getValue();
        xy_file                = _xy_file.getValue();
        symbol_style           = _symbol_style.getValue();
        symbol_size            = _symbol_size.getValue();
        bds_mark_zero          = _bds_mark_zero.getValue();
        ses                    = _ses.getValue();
        zf_config_str          = _zf_config.getValue();
#ifdef MK_EXP
        schur_str              = _schur.getValue();
        J_matrix               = _J_matrix.getValue();
        bdsbnr_str             = _bdsbnr.getValue();
        gbnr_p_str             = _gbnr_p.getValue();
#endif
        if ( _nr.isSet() )
            range_type = "nr";
        else if ( _bnr.isSet() )
            range_type = "bnr";
        else if ( _ev.isSet() )
            range_type = "ev";
        else if ( _bds.isSet() )
            range_type = "bds";
#ifdef MK_EXP
        else if ( _gbnr.isSet() )
            range_type = "gbnr";
#endif
        else if ( _info.isSet() )
            range_type = "info";

        if ( range_type == "bds" )
            lambda = _bds.getValue();

        if ( _eval.isSet() )
        {
            eval = true;
            z = _eval.getValue();
        }

    } catch (ArgException &e)  // catch any exceptions
    {
        cerr << "error: " << e.error() << " for arg " << e.argId() << endl;
    }
    
    post_process();
}

void mfrange_args::post_process()
{
    if ( !sub_str.empty() )
    {
        vector< int > vi = util::int_tokenize(",", sub_str);
        for ( unsigned i = 0; i < vi.size(); ++i )
            sub.insert(vi[i] - 1);
        
        sub_str.clear();
        set< int >::const_iterator i = sub.begin();
        sub_str += util::to_string((*i) + 1);
        for ( ++i; i != sub.end(); ++i )
            sub_str += "," + util::to_string((*i) + 1);
    }

    if ( !schur_str.empty() )
    {
        vector< int > vi = util::int_tokenize(",", schur_str);
        for ( unsigned i = 0; i < vi.size(); ++i )
            schur.insert(vi[i] - 1);
        
        schur_str.clear();
        set< int >::const_iterator i = schur.begin();
        schur_str += util::to_string((*i) + 1);
        for ( ++i; i != schur.end(); ++i )
            schur_str += "," + util::to_string((*i) + 1);
    }

    if ( decomposition_str.empty() )
    {
        if ( range_type == "bnr" )
            range_type = "nr";
    }
    else
    {
        if ( decomposition_str.find("x",0) != string::npos )
        {
            vector< int > vs = util::int_tokenize("x", decomposition_str);
            int c = 0;
            for ( unsigned i = 0; i < vs.size(); ++i )
            {
                set< int > si;
                for ( int j = 0; j < vs[i]; ++j, ++c )
                    si.insert(c);
                decomposition.push_back(si);
            }
        }
        else
        {
            vector< string > vs = util::string_tokenize(":", decomposition_str);
            for ( unsigned i = 0; i < vs.size(); ++i )
            {
                vector< int > vi = util::int_tokenize(",", vs[i]);
                set< int > si;
                for ( unsigned j = 0; j < vi.size(); ++j )
                    si.insert(vi[j] - 1);
                decomposition.push_back(si);
            }
        }
        
//         decomposition_str.clear();
//         for ( unsigned i = 0; i < decomposition.size(); ++i )
//         {
//             set< int >::const_iterator j = decomposition[i].begin();
//             decomposition_str += util::to_string((*j) + 1);
//             for ( ++j; j != decomposition[i].end(); ++j )
//                 decomposition_str += "," + util::to_string((*j) + 1);
// 
//             if ( i + 1 < decomposition.size() )
//                 decomposition_str += ":";
//         }       
    }

    if ( !bounds_str.empty() )
    {
        vector< double > bounds = util::double_tokenize(":", bounds_str);
assert( bounds.size() == 4 );
        xmin = bounds[0];
        xmax = bounds[1];
        ymin = bounds[2];
        ymax = bounds[3];

        bounds_str = string("[" + 
                     util::to_string(bounds[0]) + "," +
                     util::to_string(bounds[1]) + "]x[" +
                     util::to_string(bounds[2]) + "," +
                     util::to_string(bounds[3]) + "]");
    }

    if ( !bdsbnr_str.empty() )
    {
        vector< string > B = util::string_tokenize(":", bdsbnr_str);
assert( B.size() == 4 );
        bdsbnr_xres = atoi(B[0].c_str());
        bdsbnr_yres = atoi(B[1].c_str());
        bdsbnr_eps = atof(B[2].c_str());
        bdsbnr_max_iter = atoi(B[3].c_str());
    }

    if ( !png_size_str.empty() )
    {
        vector< int > size = util::int_tokenize(":", png_size_str);
assert( size.size() == 2 );
        png_width = size[0];
        png_height = size[1];
    }
    
    if ( !gbnr_p_str.empty() )
        gbnr_p = util::double_tokenize(",", gbnr_p_str);

    if ( zf_config_str.empty() )
    {
        char *zf_config_env = getenv("MFRANGE_ZF_CONFIG");
        if ( zf_config_env != NULL )
            zf_config_str = zf_config_env;
    }

    if ( !zf_config_str.empty() )
    {
        vector< string > S = util::string_tokenize(":", zf_config_str);
assert( S.size() >= 4 );
        double max_rect = atof(S[0].c_str()),
               line_length = atof(S[1].c_str()),
               precision = atof(S[2].c_str());
        int subdivisions = atoi(S[3].c_str());

        floatZF::set_config(max_rect, line_length, precision, subdivisions);
        
        if ( S.size() > 4 && S[4] == "1" )
            floatZF::DEBUG = true;
    }
}
