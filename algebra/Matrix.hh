// Matrix.hh

#ifndef __Matrix_hh
#define __Matrix_hh

#include <iostream>
#include <config.hh>
#include <set>

#define DEFINE_MATRIX_CONSTRUCTORS(NAME,T) \
    NAME() : Matrix<T>() {} \
    NAME(const int &n, const int &m) : Matrix<T>(n,m) {} \
    NAME(const int &n) : Matrix<T>(n) {} \
    NAME(T *entries, const int &n, const int &m) : Matrix<T>(entries,n,m) {} \
    NAME(T *entries, const int &n) : Matrix<T>(entries,n) {} \
    NAME(const Matrix< T > &other) : Matrix<T>(other) {}

template < class T >
class Matrix {

        public:
        /// empty matrix
        Matrix();

        /// \f$n \times m\f$ matrix filled with 0s
        Matrix(const int &n, const int &m);

        /// \f$n \times n\f$ matrix filled with 0s
        Matrix(const int &n);

        /// \f$n \times m\f$ matrix filled row by row with \p entries
        Matrix(T *entry, const int &n, const int &m);

        /// \f$n \times n\f$ matrix filled row by row with \p entries
        Matrix(T *entry, const int &n);

        /// Clone a matrix
        Matrix(const Matrix &other);

        // The destructor
        ~Matrix();

        /// Assignment operator
        const Matrix &operator = (Matrix other);

/// @name Testing equality
//@{ 
        const bool operator == ( const Matrix &other ) const;
        const bool operator != ( const Matrix &other ) const;
///@}

/// \name Testing for empty matrices 
//@{ 
        const bool is_empty() const;
        const bool isnt_empty() const;
        /// The 'null' in \p is_null() should be read as 'NULL', not '0'...
        const bool is_null() const;
        const bool isnt_null() const;
//@}

/// @name Information
//@{ 
        const int &rows() const;
        const int &columns() const;
        /// Only works for quadratic matrices!
        const int &dimension() const;
        /// Note: The empty matrix is not considered quadratic!
        const bool is_quadratic() const;
//@}

/// @name Access the entries
//@{
        /// The \f$k\f$-th entry, counting from top left to bottom right (for assignment)
        T &operator [] (const int &k);

        /// The \f$k\f$-th entry (const version)
        const T &operator [] (const int &k) const;

        /// The entry \f$a_{ij}\f$ (for assignment)
        T &operator () (const int &i, const int &j);

        /// The entry \f$a_{ij}\f$ (const version)
        const T &operator () (const int &i, const int &j) const;
//@}

/// @name Submatrices
//@{
        /// The submatrix \f$(a_{ij})_{i=i_1,\dots,i_2,j=j_1,\dots,j_2}\f$
        const Matrix operator () (const int &i_1, const int &j_1, const int &i_2, const int &j_2) const;
        const Matrix sub(const int &i_1, const int &j_1, const int &i_2, const int &j_2) const;
        /// The submatrix \f$(a_{r(i)c(j)})_{i=1..\#r,j=1,\dots,\#c}\f$
        const Matrix operator () (const std::set< int > &R, const std::set< int > &C) const;
        const Matrix operator () (const std::set< int > &K) const;
        const Matrix sub(const std::set< int > &R, const std::set< int > &C) const;
        const Matrix sub(const std::set< int > &K) const;

        const std::set< int > complement_row(const std::set< int > &K) const;
        const std::set< int > complement_column(const std::set< int > &K) const;
        const std::set< int > complement(const std::set< int > &K) const;
//@}

    protected:
        int r, c;
        T *entry;

        void entry_copy(T *dest, const T *src, const int &count);

        /// Assumes that r and c have been set already.
        /// Creates space for \p this->entry and fills it with \p entry
        /// (if \p entries != NULL)
        /// Also deletes \p this->entry if != NULL.
        void entry_init(const T *entry = NULL);

        /// should be defined somewhere for every type used.
        void to_zero(T &t);
};

// *************************************************************************** STRUCTORS

template < class T >
Matrix< T >::Matrix()
: entry(NULL)
{
    /// Note: The size of \p entry is now r*c = -1 < 0.
    /// (This is useful sometimes!)
    r = -1;
    c = 1;
}

template < class T >
Matrix< T >::Matrix(const int &n)
: entry(NULL)
{ 
    ASSERT(n >= 0, "");
    r = c = n;
    entry_init();
}  

template < class T >
Matrix< T >::Matrix(T *entry, const int &n)
: entry(NULL)
{ 
    ASSERT(n >= 0, "");
    r = c = n;
    entry_init(entry);
}  

template < class T >
Matrix< T >::Matrix(const int &n, const int &m)
: entry(NULL)
{ 
    ASSERT(n >= 0, "");
    ASSERT(m >= 0, "");
    r = n;
    c = m;
    entry_init();
}  

template < class T >
Matrix< T >::Matrix(T *entry, const int &n, const int &m)
: entry(NULL)
{ 
    ASSERT(n >= 0, "");
    ASSERT(m >= 0, "");
    r = n;
    c = m;
    entry_init(entry);
}  

template < class T >
Matrix< T >::Matrix(const Matrix< T > &other)
: entry(NULL)
{
    r = other.r;
    c = other.c;
    entry_init(other.entry);
}  

template < class T >
Matrix< T >::~Matrix() 
{ 
    delete[] entry;
}

template < class T >
const Matrix< T > &Matrix< T >::operator = (Matrix< T > other)
    // Note: if other is given as a reference the assignment 'A = A' will not work!
{
    r = other.r;
    c = other.c;
    entry_init(other.entry);

    return (*this);
}

// *************************************************************************** INFO

template < class T >
const int &Matrix< T >::rows() const 
{
    return r;
}

template < class T >
const int &Matrix< T >::columns() const
{
    return c;
} 

template < class T >
const int &Matrix< T >::dimension() const 
{
    ASSERT(is_quadratic(), (*this));
    return c;
}

template < class T >
const bool Matrix< T >::is_quadratic() const
{
    return ( isnt_empty() && r == c );
}

template < class T >
const bool Matrix< T >::is_empty () const
{
    return ( r == -1 );
}

template < class T >
const bool Matrix< T >::isnt_empty () const
{
    return !is_empty();
}

template < class T >
const bool Matrix< T >::is_null () const
{
    return is_empty();
}

template < class T >
const bool Matrix< T >::isnt_null () const
{
    return !is_null();
}



// *************************************************************************** (), []

template < class T >
T &Matrix< T >::operator [] (const int &k)
{
    ASSERT(0 <= k && k < r*c, SHOW_VAR(k) AND_SHOW_VAR(*this));
    return entry[k];
}

template < class T >
const T &Matrix< T >::operator [] (const int &k) const
{
    ASSERT(0 <= k && k < r*c, SHOW_VAR(k) AND_SHOW_VAR(*this));
    return entry[k];
}

template < class T >
T &Matrix< T >::operator () (const int &i, const int &j)
{
    ASSERT(0 <= i && i < r && 0 <= j && j < c,
           SHOW_VAR(i) AND_SHOW_VAR(j) AND_SHOW_VAR(*this));
    return entry[i*c + j];
}

template < class T >
const T &Matrix< T >::operator () (const int &i, const int &j) const
{ 
    ASSERT(0 <= i && i < r && 0 <= j && j < c,
           SHOW_VAR(i) AND_SHOW_VAR(j) AND_SHOW_VAR(*this));
    return entry[i*c + j];
}

// *************************************************************************** ==

template < class T >
const bool Matrix< T >::operator == (const Matrix< T > &other) const
{
    if ( c != other.c || r != other.r ) 
        return false;
    for ( int i = 0; i < r*c; i++ )
        if ( entry[i] != other.entry[i] ) 
            return false;
    return true;
}  	

template < class T >
const bool Matrix< T >::operator != (const Matrix< T > &other) const
{
    return !( (*this) == other );
}  	

// *************************************************************************** SUBMATRICES

template < class T >
const Matrix< T > Matrix< T >::operator ()
            (const int &i1, const int &j1, const int &i2, const int &j2) const
{
    ASSERT(0 <= i1 && i1 <= i2 && i2 < r && 0 <= j1 && j1 <= j2 && j2 < c,
           SHOW_VAR(i1) AND_SHOW_VAR(j1) AND_SHOW_VAR(i2) AND_SHOW_VAR(j2) AND_SHOW_VAR((*this)));

    Matrix< T > result ( i2 - i1 + 1, j2 - j1 + 1 );
    for ( int i = i1; i <= i2; i ++ )
        for ( int j = j1; j <= j2; j++ )
            result(i - i1, j - j1) = (*this)(i, j);
    return result;
}

template < class T >
const Matrix< T > Matrix< T >::sub
            (const int &i1, const int &j1, const int &i2, const int &j2) const
{
    ASSERT(0 <= i1 && i1 <= i2 && i2 < r && 0 <= j1 && j1 <= j2 && j2 < c,
           SHOW_VAR(i1) AND_SHOW_VAR(j1) AND_SHOW_VAR(i2) AND_SHOW_VAR(j2) AND_SHOW_VAR(*this));
    return (*this)(i1,j1,i2,j2);
}

template < class T >
const Matrix< T > Matrix< T >::operator () (const std::set< int > &R, const std::set< int > &C) const
{
    Matrix< T > S(R.size(), C.size());
    int i, j;
    std::set< int >::const_iterator iR, jC;
    for ( iR = R.begin(), i = 0; iR != R.end(); ++iR, ++i)
        for ( jC = C.begin(), j = 0; jC != C.end(); ++jC, ++j)
        {
            ASSERT(0 <= (*iR) && (*iR) < r && 0 <= (*jC) && (*jC) < c,
                   SHOW_VAR(*iR) AND_SHOW_VAR(*jC) AND_SHOW_VAR(*this));
            S(i,j) = (*this)(*iR,*jC);
        }
    return S;
}

template < class T >
const Matrix< T > Matrix< T >::sub(const std::set< int > &R, const std::set< int > &C) const
{
    return (*this)(R,C);
}

template < class T >
const Matrix< T > Matrix< T >::operator () (const std::set< int > &K) const
{
    return (*this)(K,K);
}

template < class T >
const Matrix< T > Matrix< T >::sub(const std::set< int > &K) const
{
    return (*this)(K,K);
}

template < class T >
const std::set< int > Matrix< T >::complement_row(const std::set< int > &K) const
{
    std::set< int > cK;
    for ( int i = 0; i < r; ++i )
        if ( K.find(i) == K.end() )
            cK.insert(i);
    return cK;
}

template < class T >
const std::set< int > Matrix< T >::complement_column(const std::set< int > &K) const
{
    std::set< int > cK;
    for ( int i = 0; i < c; ++i )
        if ( K.find(i) == K.end() )
            cK.insert(i);
    return cK;
}

template < class T >
const std::set< int > Matrix< T >::complement(const std::set< int > &K) const
{
    ASSERT(is_quadratic(), SHOW_VAR(*this));
    return complement_row(K);
}

// *************************************************************************** INTERNALS


template < class T >
void Matrix< T >::entry_copy(T *dest, const T *src, const int &count)
{
/** \internal
    memcpy will \b not work for more complicated types as classes, 
    e.g. using within Matrix<Polynomial> will cause a segmentation fault
    sooner or later!!! (Or produce some stupid results at least...)
*/

//          memcpy ( dest, src, count * sizeof (T) );
    for ( int i = 0; i < count; ++i )
        dest[i] = src[i];
}

template < class T >
void Matrix< T >::entry_init(const T *entry)
{
    ASSERT(r >= 0, "");
    ASSERT(c >= 0, "");

    if ( this->entry != NULL )
        delete[] this->entry;
    
    if ( r == 0 || c == 0 )
        this->entry = NULL;
    else
    {
        this->entry = new T[r*c];

        if ( entry == NULL )
            for ( int i = 0; i < r*c; i++ )
                to_zero(this->entry[i]);
        else
            entry_copy(this->entry, entry, r*c);
    }
}

// *************************************************************************** IO

template < class T >
std::ostream &operator << (std::ostream &out, const Matrix< T > &m)
{
    if ( m.is_null() )
        return out << "(null)" << std::flush;
    else if ( m.rows() == 0 || m.columns() == 0 )
        return out << m.rows() << "x" << m.columns() << std::endl;
    else
    {
        for ( int i = 0; i < m.rows(); i++ )
        { 
            out << "\n";
            for ( int j = 0; j < m.columns(); j++ )
                out << m(i,j) << " ";
        }
        return out << std::endl;
    }
}      

template < class T >
std::istream &operator >> (std::istream &in, Matrix< T > &m)
{
    for ( int i = 0; i < m.rows(); i++ )
        for ( int j = 0; j < m.columns(); j++ )
            if ( !( in >> m (i, j) ) ) 
                return in;
    return in;
}

#endif
