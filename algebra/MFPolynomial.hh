// MFPolynomial.hh

#ifndef __MFPolynomial_hh
#define __MFPolynomial_hh

#include <iostream>
#include <vector>
#include <string>
#include <MatrixFunction.hh>
#include <MatrixPolynomial.hh>

/// A MatrixFunction of the form \f$z \mapsto A_d z^d + \dots + A_1 z + A_0\f$

class MFPolynomial 
:   public MatrixFunction,
    public MatrixPolynomial
{
    public:
        MFPolynomial(const MatrixPolynomial &P);
        virtual const std::string type() const;
        void write(std::ostream &out) const;
        const int dimension() const;
        const ComplexMatrix eval(const COMPLEX &z);
        MatrixFunction *sub_function(const std::set< int > &K);
        const COMPLEX eval(const int &i, const int &j, const COMPLEX &z);
        const std::vector< COMPLEX > block_eigenvalues
            (const std::vector< std::set< int > > D, 
             const std::vector< ComplexVector > &x, const std::vector< ComplexVector > &y);
        const std::vector< COMPLEX > eigenvalues();
};

#endif
