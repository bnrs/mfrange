// ComplexMatrix.cc

#include <ComplexMatrix.hh>
#include <cassert>
#include <netlib.hh>

// Don't divide by numbers of (maximum) norm less than ZERO in
// function ComplexMatrix::eigenvalues(...):
#define ZERO 1e-18

using namespace std;

// *************************************************************************** STATICS

ComplexMatrix ComplexMatrix::id(const int &n)
{
    ASSERT(n > 0, "");
    ComplexMatrix result(n);
    for ( register int i = 0; i < n; i++ )
        result(i,i) = COMPLEX (1);
    return result;
}

ComplexMatrix ComplexMatrix::zero(const int &n)
{
    ASSERT(n >= 0, "");
    ComplexMatrix result(n);
    return result;
}

// *************************************************************************** CONVENIENCE

const ComplexMatrix ComplexMatrix::id() const
{
    ASSERT(isnt_empty(), "");
    ASSERT(is_quadratic(), SHOW_VAR(*this));
    ASSERT(dimension() > 0, "");
    return ComplexMatrix::id(dimension());
}

const ComplexMatrix ComplexMatrix::zero() const 
{
    ASSERT(isnt_empty(), "");
    ASSERT(is_quadratic(), SHOW_VAR(*this));
    return ComplexMatrix::zero(dimension());
}

// *************************************************************************** +, -

const ComplexMatrix ComplexMatrix::operator +=(const ComplexMatrix &other)
{
    ASSERT(isnt_empty(), "");
    ASSERT(r == other.rows() && c == other.columns(), 
           SHOW_VAR(*this) AND_SHOW_VAR(other));

    for ( int k = 0; k < r*c; ++k )
        entry[k] += other[k];
    return *this;
}

const ComplexMatrix ComplexMatrix::operator +(const ComplexMatrix &other) const
{
    ASSERT(isnt_empty(), "");
    ASSERT(r == other.rows() && c == other.columns(), 
           SHOW_VAR(*this) AND_SHOW_VAR(other));

    ComplexMatrix R(*this);
    return R += other;
}

const ComplexMatrix ComplexMatrix::operator -=(const ComplexMatrix &other)
{
    ASSERT(isnt_empty(), "");
    ASSERT(r == other.rows() && c == other.columns(), 
           SHOW_VAR(*this) AND_SHOW_VAR(other));

    for ( int k = 0; k < r*c; ++k )
        entry[k] -= other[k];
    return *this;
}

const ComplexMatrix ComplexMatrix::operator -(const ComplexMatrix &other) const
{
    ASSERT(isnt_empty(), "");
    ASSERT(r == other.rows() && c == other.columns(), 
           SHOW_VAR(*this) AND_SHOW_VAR(other));

    ComplexMatrix R(*this);
    return R -= other;
}

// *************************************************************************** SCALAR *

const ComplexMatrix ComplexMatrix::operator *=(const COMPLEX &z)
{
    ASSERT(isnt_empty(), "");

    for ( int k = 0; k < r*c; ++k )
        entry[k] *= z;
    return *this;
}

const ComplexMatrix ComplexMatrix::operator *(const COMPLEX &z) const
{
    ASSERT(isnt_empty(), "");

    ComplexMatrix R(*this);
    return R *= z;
}

// *************************************************************************** FUNCTIONS

const ComplexVector ComplexMatrix::operator *(const ComplexVector &v) const
{
    ASSERT(isnt_empty(), "");
    ASSERT(c == v.dimension(), 
           SHOW_VAR(*this) AND_SHOW_VAR(v));

    ComplexVector result(r);
    for ( int j = 0; j < r; ++j )
        for ( int i = 0; i < c; ++i ) 
            result(j) += (*this)(j,i) * v(i); 
  return result;
}

const ComplexMatrix ComplexMatrix::operator *(const ComplexMatrix &other) const
{
    ASSERT(isnt_empty(), "");
    ASSERT(c == other.r,
           SHOW_VAR(*this) AND_SHOW_VAR(other));

    ComplexMatrix result(r,other.c);
    for ( int i = 0; i < r; ++i )
        for ( int j = 0; j < other.c; ++j ) 
            for ( int k = 0; k < c; ++k )
                result(i,j) += (*this)(i,k) * other(k,j); 
    return result;
}

const COMPLEX ComplexMatrix::det() const
{
    ASSERT(isnt_empty(), "");
    ASSERT(is_quadratic(), SHOW_VAR(*this));
    ASSERT(dimension() > 0, "")

    if ( dimension() == 1 )
        return entry[0];
    else if ( dimension() == 2 )
        return (*this)(0,0)*(*this)(1,1) - (*this)(0,1)*(*this)(1,0);

    using namespace netlib;

    const ComplexMatrix &A = (*this);
    long int dim = A.columns();
    cplx _A[dim][dim];
    long int ipvt[dim];
    long int info;
    
    for ( int i = 0; i < dim; ++i )
        for ( int j = 0; j < dim; ++j )
        {
            _A[i][j].r = A(i,j).real();
            _A[i][j].i = A(i,j).imag();
        }

    CGEFA(&_A[0][0], &dim, &dim, &ipvt[0], &info);

    cplx work[dim];
    long int job = 10;
    cplx _det[2];
    CGEDI(&_A[0][0], &dim, &dim, &ipvt[0], &_det[0], &work[0], &job);
    
    return COMPLEX(_det[0].r,_det[0].i)*pow(10.0, COMPLEX(_det[1].r,_det[1].i));
}

const vector< COMPLEX > ComplexMatrix::eigenvalues() const
{
    ASSERT(isnt_empty(), "");
    ASSERT(is_quadratic(), SHOW_VAR(*this));
    
    if ( dimension() == 0 )
        return vector< COMPLEX >();

    return eigenvalues(id(r));
}

const ComplexMatrix ComplexMatrix::adjoint() const
{
    ComplexMatrix B(c,r);

    for ( int i = 0; i < r; ++i )
        for ( int j = 0; j < c; ++j )
            B(j,i) = conj((*this)(i,j));
    return B;
}

const DOUBLE ComplexMatrix::norm() const
{
    vector<COMPLEX> e = (adjoint()*(*this)).eigenvalues();
    DOUBLE M = 0, m = 0;
    
    for ( unsigned i = 0; i < e.size(); ++i )
        if ( (m = abs(e[i])) > M ) M = m;

    return M;
}


const vector< COMPLEX > ComplexMatrix::eigenvalues(const ComplexMatrix &B) const
{
    ASSERT(isnt_empty(), "");
    ASSERT(is_quadratic(), SHOW_VAR(*this));
    ASSERT(dimension() > 0, "");
    ASSERT(B.is_quadratic(), SHOW_VAR(B));
    ASSERT(dimension() == B.dimension(), SHOW_VAR(*this) AND_SHOW_VAR(B));

    using namespace netlib;

    const ComplexMatrix &A = (*this);
    vector< COMPLEX > r;
    long int dim = A.columns();
    vector<COMPLEX> a, b;

    cplx _A[dim][dim];
    cplx _B[dim][dim];
    cplx alpha[dim];
    cplx beta[dim]; 

    for ( int i = 0; i < dim; ++i )
    {   for ( int j = 0; j < dim; ++j ) 
        { 
            _A[i][j].r = A(i,j).real();
            _A[i][j].i = A(i,j).imag();
            _B[i][j].r = B(i,j).real();
            _B[i][j].i = B(i,j).imag();
        }
    }

    long int l = 0; // don't need eigenvectors.
    cplx x[dim][dim];
    long int iter[dim];

    LZHES (&dim,&_A[0][0],&dim,&_B[0][0],&dim,&x[0][0],&dim,&l);
    LZIT (&dim,&_A[0][0],&dim,&_B[0][0],&dim,&x[0][0],&dim,&l,&iter[0],&alpha[0],&beta[0]);

    bool all_ok = true;

    for ( int i = 0; i < dim; ++i )
        if ( iter[i] == -1 )
        {
            all_ok = false;
            ev_fiter++;
            break;
        }

    if ( all_ok )
        for ( int i = 0; i < dim; ++i )
        {
            a.push_back(COMPLEX(alpha[i].r,alpha[i].i));
            b.push_back(COMPLEX(beta[i].r,beta[i].i));
        }

    for ( unsigned i = 0; i < a.size(); ++i)
    {
        if ( abs(b[i].real()) < ZERO && abs(b[i].imag()) < ZERO )
            ev_fZERO++;
        else
        {
            COMPLEX e = a[i]/b[i];
            if ( isnan(e.real()) || isnan(e.imag()) )
                ev_fnan++;
            else
                r.push_back(e);
        }
    }

    return r;
}

const ComplexMatrix ComplexMatrix::inverse() const
{
    ASSERT(isnt_empty(), "");
    ASSERT(is_quadratic(), SHOW_VAR(*this));

    if ( dimension() == 0 )
        return (*this);

    using namespace netlib;

    const ComplexMatrix &A = (*this);
    long int dim = A.columns();
    cplx _A[dim][dim];
    long int ipvt[dim];
    long int info;
    
    for ( int i = 0; i < dim; ++i )
        for ( int j = 0; j < dim; ++j )
        {
            _A[i][j].r = A(i,j).real();
            _A[i][j].i = A(i,j).imag();
        }

    CGEFA(&_A[0][0], &dim, &dim, &ipvt[0], &info);

    cplx work[dim];
    long int job = 01;
    cplx _det[2];
    CGEDI(&_A[0][0], &dim, &dim, &ipvt[0], &_det[0], &work[0], &job);
    
    ComplexMatrix R(dim);

    for ( int i = 0; i < dim; ++i )
        for ( int j = 0; j < dim; ++j )
            R(i,j) = COMPLEX(_A[i][j].r,_A[i][j].i);
    
    return R;
}

const ComplexMatrix ComplexMatrix::block_sp(const vector< ComplexVector > &x) const
{
    ASSERT(isnt_empty(), "");
    ASSERT(is_quadratic(), SHOW_VAR(*this));

    int d = x.size();
    int s[d + 1];

    s[0] = 0;
    for ( int k = 0; k < d; ++k )
    {
        ASSERT(x[k].dimension() > 0, SHOW_VAR(x[k]));
        s[k + 1] = s[k] + x[k].dimension();
    }
    
    ASSERT(dimension() == s[d], SHOW_VAR(*this)AND_SHOW_VAR(s[d]));

    ComplexMatrix R(d);
    
    for ( int i = 0; i < d; ++i )
        for ( int j = 0; j < d; ++j )
            R(i,j) = (ComplexMatrix)sub(s[i],s[j],s[i + 1] - 1,s[j + 1] - 1)*x[j]|x[i];

    return R;
}

const ComplexMatrix ComplexMatrix::block_sp(const vector< set< int > > &D,
                                            const vector< ComplexVector > &x,
                                            const vector< ComplexVector > &y) const
{
    ASSERT(isnt_empty(), "");
    ASSERT(is_quadratic(), SHOW_VAR(*this));
    
    if ( D.empty() )
    {
        ASSERT(x.size() == 1, SHOW_VAR(x.size()));
        ASSERT(y.size() == 1, SHOW_VAR(y.size()));
        return id(1)*(((*this)*x[0])|y[0]);
    }
    
    ASSERT(D.size() == x.size(), SHOW_VAR(D.size())AND_SHOW_VAR(x.size()));
    ASSERT(D.size() == y.size(), SHOW_VAR(D.size())AND_SHOW_VAR(y.size()));

    int nD = D.size();
    ComplexMatrix R(nD);
    
    for ( int i = 0; i < nD; ++i )
        for ( int j = 0; j < nD; ++j )
        {
            // ASSERT(...,"")
            R(i,j) = ((ComplexMatrix)sub(D[i],D[j]))*x[j]|y[i];
        }
    return R;
}

const ComplexMatrix ComplexMatrix::block_sp(
    const std::vector< std::set< int > > &D,
    const std::vector< ComplexVector > &x) const
{
    ASSERT(isnt_empty(), "");
    ASSERT(is_quadratic(), SHOW_VAR(*this));
    
    return block_sp(D, x, x);
}

const ComplexMatrix ComplexMatrix::schur_complement(const int &k1, const int &k2) const
{
    ASSERT(isnt_empty(), "");
    ASSERT(is_quadratic(), SHOW_VAR(*this));
    ASSERT(0 <= k1 && k1 <= k2 && k2 < dimension(), 
           SHOW_VAR(k1) AND_SHOW_VAR(k2) AND_SHOW_VAR(*this));
    
    set< int > K;
    for ( int k = k1; k <= k2; ++k )
        K.insert(k);
    
    return schur_complement(K);
}

const ComplexMatrix ComplexMatrix::schur_complement(const set< int > &K) const
{
    ASSERT(isnt_empty(), "");
    ASSERT(is_quadratic(), SHOW_VAR(*this));

    set< int > cK = complement(K);
    ASSERT(K.size() + cK.size() == (unsigned) dimension(),
           SHOW_VAR(K.size()) AND_SHOW_VAR(cK.size()) AND_SHOW_VAR(*this));

    ComplexMatrix S11 = sub(K,  K), S12 = sub(K, cK),
                  S21 = sub(cK, K), S22 = sub(cK,cK);
    return S11 - S12*S22.inverse()*S21;
}


// static varibales:
long ComplexMatrix::ev_fiter = 0;
long ComplexMatrix::ev_fZERO = 0;
long ComplexMatrix::ev_fnan = 0;
long ComplexMatrix::ev_finfo = 0;



