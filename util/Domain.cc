// Domain.cc

#include <Domain.hh>

using namespace std;

// ********************************************************** BOX ******

Box::Box(const double &xmin, const double &xmax, 
         const double &ymin, const double &ymax)
{
    this->xmin = xmin;
    this->xmax = xmax;
    this->ymin = ymin;
    this->ymax = ymax;
}

const bool Box::operator<(const Box &b) const
{
    return (
            xmin < b.xmin || ( xmin == b.xmin && ymin < b.ymin )
           )
           ||
           (
            xmin == b.xmin && ymin == b.ymin 
              && (xmax < b.xmax || ( xmax == b.xmax && ymax < b.ymax ))
           )
           ;
}

const bool Box::has_interior() const
{
    return ( xmin < xmax && ymin < ymax );
}

const bool Box::contains(const COMPLEX &z) const
{
    return ( xmin <= z.real() && z.real() <= xmax &&
             ymin <= z.imag() && z.imag() <= ymax );
}

set< Box > Box::remove(const Box &b) const
{
    double rxmin, rxmax, rymin, rymax;
    set< Box > R;
    
    rxmin = ( b.xmin < xmin ) ? xmin : b.xmin;
    rymin = ( b.ymin < ymin ) ? ymin : b.ymin;
    rxmax = ( b.xmax > xmax ) ? xmax : b.xmax;
    rymax = ( b.ymax > ymax ) ? ymax : b.ymax;
    
    if ( rxmin < rxmax && rymin < rymax )
    {
        Box b[4] = {Box(xmin,rxmin,ymin,ymax),   // left
                    Box(rxmin,rxmax,ymin,rymin), // lower
                    Box(rxmin,rxmax,rymax,ymax), // upper
                    Box(rxmax,xmax,ymin,ymax)};  // right
        for ( int i = 0; i < 4; ++i )
            if ( b[i].has_interior() )
                R.insert(b[i]);
    }
    else
        R.insert(*this);
    
    return R;
}

const double Box::max_side() const
{
    double dx = xmax - xmin,
           dy = ymax - ymin;
    return dx > dy ? dx : dy;
}

const COMPLEX Box::middle() const
{
    return .5*COMPLEX(xmin + xmax, ymin + ymax);
}


ostream &operator<<(ostream &out, const Box &b)
{
    if ( !b.has_interior() )
        return out << b.xmin << " " << b.xmin << " " << b.ymin << " " << b.ymin;
    else
        return out << b.xmin << " " << b.xmax << " " << b.ymin << " " << b.ymax;
}


// ********************************************************** DOMAIN ******


Domain::Domain() {}

Domain::Domain(const Box &b)
{
    insert(b);
}

Domain::Domain(const set< Box > &b)
: set< Box >(b) {}


Domain &Domain::add(const Box &b)
{
    insert(b);
    return *this;
}

Domain &Domain::add(const Domain &d)
{
    insert(d.begin(),d.end());
    return *this;
}

Domain &Domain::remove(const Box &b)
{
    Domain d;
    for ( const_iterator i = begin(); i != end(); ++i )
        d.add(i->remove(b));
    
    (*this) = d;
    return *this;
}

Domain &Domain::remove(const Domain &d)
{
    for ( const_iterator i = d.begin(); i != d.end(); ++i )
        remove(*i);
    return *this;
}

const bool Domain::contains(const COMPLEX &z) const
{
    for ( const_iterator i = begin(); i != end(); ++i )
        if ( i->contains(z) )
            return true;
    
    return false;
}


ostream &operator<<(ostream &out, const Domain &d)
{
    for ( Domain::const_iterator i = d.begin(); i != d.end(); ++i )
        out << (*i) << endl;
    return out;
}

