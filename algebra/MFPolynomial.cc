// MFPolynomial.cc

#include <cassert>
#include <MFPolynomial.hh>

using namespace std;

MFPolynomial::MFPolynomial(const MatrixPolynomial &P)
: MatrixPolynomial(P)
{
    ASSERT(P.is_quadratic(), SHOW_VAR(P));
}

const std::string MFPolynomial::type() const
{
    return "Polynomial";
}

const int MFPolynomial::dimension() const
{
    return MatrixPolynomial::dimension();
}

const ComplexMatrix MFPolynomial::eval(const COMPLEX &z)
{
    return MatrixPolynomial::eval(z);
}

MatrixFunction *MFPolynomial::sub_function(const std::set< int > &K)
{
    return new MFPolynomial(sub(K,K));
}

const COMPLEX MFPolynomial::eval(const int &i, const int &j, const COMPLEX &z)
{
    return eval(z)(i,j);
}

const std::vector< COMPLEX > MFPolynomial::block_eigenvalues
        (const vector< set< int > > D, 
         const vector< ComplexVector > &x, const std::vector< ComplexVector > &y)
{
    return block_sp(D, x, y).eigenvalues();
}

const std::vector< COMPLEX > MFPolynomial::eigenvalues()
{
    return MatrixPolynomial::eigenvalues();
}
