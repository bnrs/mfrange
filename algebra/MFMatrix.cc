// MFMatrix.cc

#include <cassert>
#include <MFMatrix.hh>

using namespace std;

MFMatrix::MFMatrix(const ComplexMatrix &A)
: ComplexMatrix(A) 
{}

const std::string MFMatrix::type() const
{
    return "Matrix";
}

const int MFMatrix::dimension() const
{
    return ComplexMatrix::dimension();
}

const ComplexMatrix MFMatrix::eval(const COMPLEX &z)
{
    return (*this) - id()*z;
}

MatrixFunction *MFMatrix::sub_function(const std::set< int > &K)
{
    return new MFMatrix((*this)(K,K));
}

const std::vector< COMPLEX > MFMatrix::block_eigenvalues
        (const vector< set< int > > D, 
         const vector< ComplexVector > &x, const std::vector< ComplexVector > &y)
{
    return block_sp(D, x, y).eigenvalues();
}

const std::vector< COMPLEX > MFMatrix::eigenvalues()
{
    return ComplexMatrix::eigenvalues();
}

