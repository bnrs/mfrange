// util.hh

#ifndef __util_hh
#define __util_hh

#include <iostream>
#include <sstream>
#include <vector>
#include <config.hh>

/// Collection of some helpful functions

namespace util
{
    using namespace std;

    template <class T>
    const string to_string(const T &x)
    {
        
// cerr << __FILE__ << ", " << __LINE__ << ": " << x << endl;
        string s;
// cerr << __FILE__ << ", " << __LINE__ << ": " << s << endl;
        stringstream out;
        out.precision(cout.precision());
        out << x << endl;
        out >> s;
// cerr << __FILE__ << ", " << __LINE__ << ": " << s << endl;
        return s;    
    }

    /// Returns the bounds of the set of complex points given by \p p
    /// in the form xmin, xmax, ymin, ymax
    const vector< DOUBLE > get_bounds(const vector< COMPLEX > &p);

    vector< string > string_tokenize(const string &delimiters, const string &s);
    vector< int > int_tokenize(const string &delimiters, const string &s);
    vector< float > float_tokenize(const string &delimiters, const string &s);
    vector< double > double_tokenize(const string &delimiters, const string &s);
}

#endif
