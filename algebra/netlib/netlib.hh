// netlib.hh

#ifndef __netlib_hh
#define __netlib_hh

#define LZIT lzit_
#define LZHES lzhes_
#define CGEDI cgedi_
#define CGEFA cgefa_

/// Collection of subroutines from www.netlib.org
namespace netlib
{
    class cplx { public: float r, i; };
    class dcplx { public: double r, i; };

    extern "C"
    {
        /// \p LZHES reduces the complex matrix a to upper
        /// hessenberg form and reduces the complex matrix b to
        /// triangular form
        /**
            \b Source: <A HREF="http://www.netlib.org/toms/496">http://www.netlib.org/toms/496</A>

            @param[in] N      the order of the \p A and \p B matrices
            @param[in] A      a complex matrix
            @param[in] NA     the row dimension of the \p A matrix
            @param[in] B      a complex matrix
            @param[in] NB     the row dimension of the \p B matrix
            @param[in] NX     the row dimension of the \p X matrix
            @param[in] WANTX  a logical variable which is set to .TRUE. if
                              the eigenvectors are wanted. otherwise it should
                              be set to .FALSE.
            @param[out] A     on output a is an upper hessenberg matrix, the
                              original matrix has been destroyed
            @param[out] B     an upper triangular matrix, the original matrix
                              has been destroyed
            @param[out] X     contains the transformations needed to compute
                              the eigenvectors of the original system
        **/
        void LZHES (long int *N, cplx *A, long int *NA, cplx *B, long int *NB, 
                    cplx *X, long int *NX, long int *WANTX);

        /// \p LZIT solves the generalized eigenvalue problem \n
        /// \f[A X  = \lambda B X\f]
        /// where \f$A\f$ is a complex upper hessenberg matrix of
        /// order \f$n\f$ and \f$B\f$ is a complex upper triangular matrix of order \f$n\f$.
        /**
            \b Source: <A HREF="http://www.netlib.org/toms/496">http://www.netlib.org/toms/496</A>

            @param[in] N      order of \p A and \p B
            @param[in] A      an \p N x \p N upper hessenberg complex matrix
            @param[in] NA     the row dimension of the \p A matrix
            @param[in] B      an \p N x \p N upper triangular complex matrix
            @param[in] NB     the row dimension of the \p B matrix
            @param[in] X      contains transformations to obtain eigenvectors of
                              original system. if eigenvectors are requested and \p qzhes
                              is not called, \p X should be set to the identity matrix
            @param[in] NX     the row dimension of the \p X matrix
            @param[in] WANTX  logical variable which should be set to .TRUE.
                              if eigenvectors are wanted. otherwise it
                              should be set to .FALSE.
            @param[out] X     the i-th column contains the i-th eigenvector
                              if eigenvectors are requested
            @param[out] ITER  an integer array of length \p N whose i-th entry
                              contains the number of iterations needed to find
                              the i-th eigenvalue. for any i if iter(i) =-1 then
                              after 30 iterations there has not been a sufficient
                              decrease in the last subdiagonal element of a
                              to continue iterating.
            @param[out] EIGA  a complex array of length \p N containing the diagonal of \p A
            @param[out] EIGB  a complex array of length \p N containing the diagonal of \p B
                              the i-th eigenvalue can be found by dividing \p EIGA(i) by
                              \p EIGB(i). watch out for \p EIGB(i) being zero
        **/
        void LZIT (long int *N, cplx *A, long int *NA, cplx *B, long int *NB,
                   cplx *X, long int *NX, long int *WANTX, 
                   long int *ITER, cplx *EIGA, cplx *EIGB);


        /// CGEDI computes the determinant and inverse of a matrix
        /// using the factors computed by cgeco or cgefa.
        /**
            \b Source: <A HREF="http://www.netlib.org/linpack/cgedi.f">http://www.netlib.org/linpack/cgedi.f</A>

            @param[in]  a     \p complex(lda, n) \n
                              the output from \p cgeco or \p cgefa
            @param[in]  lda   \p integer \n
                              the leading dimension of the array  \p a
            @param[in]  n     \p integer \n
                              the order of the matrix  \p a
            @param[in]  ipvt  \p integer(n) \n
                              the pivot vector from \p cgeco or \p cgefa
            @param[in]  work  \p complex(n) \n
                              work vector. contents destroyed.
            @param[in]  job   \p integer \n
                              = 11   both determinant and inverse \n
                              = 01   inverse only \n
                              = 10   determinant only \n
            @param[out] a     inverse of original matrix if requested.
                              otherwise unchanged.
            @param[out] det   \p complex(2) \n
                              determinant of original matrix if requested.
                              otherwise not referenced.
                              determinant = \p det(1) * 10.0**\p det(2)
                              with  1.0 .le. cabs1(\p det(1)) .lt. 10.0
                              or  \p det(1) .eq. 0.0 .
        **/
        void CGEDI(cplx *a, long int *lda, long int *n, long int *ipvt, cplx *det,
                   cplx *work, long int *job);

        /// \p cgefa factors a complex matrix by gaussian elimination.

        /**
            \b Source: <A HREF="http://www.netlib.org/linpack/cgefa.f">http://www.netlib.org/linpack/cgefa.f</A>

            \p cgefa is usually called by \p cgeco, but it can be called
             directly with a saving in time if  \p rcond  is not needed.
            (time for \p cgeco) = (1 + 9/\p n)*(time for \p cgefa) .

            @param[in]   a    \p complex(lda, n) \n
                              the matrix to be factored.
            @param[in]   lda  \p integer \n
                              the leading dimension of the array  a .
            @param[in]   n    \p integer \n
                              the order of the matrix  a .
            @param[out]  a    an upper triangular matrix and the multipliers
                              which were used to obtain it.
                              the factorization can be written  a = l*u  where
                              l  is a product of permutation and unit lower
                              triangular matrices and  u  is upper triangular.
            @param[out] ipvt  \p integer(n) \n
                              an integer vector of pivot indices.
            @param[out] info  \p integer \n
                              = 0  normal value \n
                              = k  if  \p u(k,k) .eq. 0.0 .  this is not an error
                                   condition for this subroutine, but it does
                                   indicate that cgesl or cgedi will divide by zero
                                   if called.  use  rcond  in cgeco for a reliable
                                   indication of singularity.
        **/
        void CGEFA(cplx *a, long int *lda, long int *n, long int *ipvt, long int *info);
    }
}

#endif
