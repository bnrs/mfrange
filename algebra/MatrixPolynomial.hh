// MatrixPolynomial.hh

#ifndef __MatrixPolynomial_hh
#define __MatrixPolynomial_hh

#include <ComplexMatrix.hh>
#include <MatrixFunction.hh>

/// Simple class representing a matrix polynomial

/** 
\note
Only very basic methods are defined.
*/

class MatrixPolynomial 
{
    public:
        MatrixPolynomial();
        MatrixPolynomial(const std::vector< ComplexMatrix > &C);
        MatrixPolynomial(const MatrixPolynomial &other);
        ~MatrixPolynomial();

        const MatrixPolynomial &operator = (MatrixPolynomial other);
        
        const bool is_empty() const;
        const bool isnt_empty() const;
        const bool is_null() const;
        const bool isnt_null() const;

        const bool is_quadratic() const;
        const int &degree() const;
        const int &rows() const;
        const int &columns() const;
        const int dimension() const;

        const MatrixPolynomial companion() const;

        const bool operator != (const MatrixPolynomial &other) const;
        const bool operator == (const MatrixPolynomial &other) const;

        const ComplexMatrix &operator [] (const int &k) const;
        ComplexMatrix &operator [] (const int &k);

        const MatrixPolynomial operator()(const std::set< int > &r, const std::set< int > &c) const;
        const MatrixPolynomial sub(const std::set< int > &r, const std::set< int > &c) const;

        const ComplexMatrix eval(const COMPLEX &z) const;
        MatrixPolynomial block_sp(const std::vector< ComplexVector > &x);
        MatrixPolynomial block_sp
            (const std::vector< std::set< int > > D, const std::vector< ComplexVector > &x);
        MatrixPolynomial block_sp
            (const std::vector< std::set< int > > D, 
             const std::vector< ComplexVector > &x, const std::vector< ComplexVector > &y);

        const std::vector< COMPLEX > eigenvalues();

    protected:
        ComplexMatrix *C;
        int deg;
};

std::ostream &operator << (std::ostream &out, const MatrixPolynomial &P);

#endif
