// util.cc

#include "util.hh"

using namespace std;

const vector< DOUBLE > util::get_bounds(const vector< COMPLEX > &p)
{
    vector< DOUBLE > r;
    if ( p.size() == 0 )
        return r;

    DOUBLE xmin = p[0].real(), xmax = p[0].real(), 
           ymin = p[0].imag(), ymax = p[0].imag();

    for ( unsigned i = 1; i < p.size(); ++i )
    {
        const DOUBLE &re = p[i].real(),
                    &im = p[i].imag();
        if ( re > xmax ) xmax = re;
        if ( re < xmin ) xmin = re;
        if ( im > ymax ) ymax = im;
        if ( im < ymin ) ymin = im;
    }
    
    r.push_back(xmin);
    r.push_back(xmax);
    r.push_back(ymin);
    r.push_back(ymax);

    return r;
}

vector< string > util::string_tokenize(const string &delimiters, const string &s)
{
    vector<string> r;
    string::size_type lastPos = s.find_first_not_of(delimiters, 0);
    string::size_type pos     = s.find_first_of(delimiters, lastPos);
    while (string::npos != pos || string::npos != lastPos)
    {
        r.push_back(s.substr(lastPos, pos - lastPos));
        lastPos = s.find_first_not_of(delimiters, pos);
        pos = s.find_first_of(delimiters, lastPos);
    }
    return r;
}

vector< float > util::float_tokenize(const string &delimiters, const string &s)
{
    vector<float> r;
    vector<string> sr = string_tokenize(delimiters, s);

    for ( unsigned i = 0; i < sr.size(); ++i )
        r.push_back(atof(sr[i].c_str()));

    return r;
}

vector< double > util::double_tokenize(const string &delimiters, const string &s)
{
    vector<double> r;
    vector<string> sr = string_tokenize(delimiters, s);

    for ( unsigned i = 0; i < sr.size(); ++i )
        r.push_back(atof(sr[i].c_str()));

    return r;
}

vector< int > util::int_tokenize(const string &delimiters, const string &s)
{
    vector<int> r;
    vector<string> sr = string_tokenize(delimiters, s);

    for ( unsigned i = 0; i < sr.size(); ++i )
        r.push_back(atoi(sr[i].c_str()));

    return r;
}
