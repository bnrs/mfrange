# 3rd Party Code

The following 3rd party code is used by mfrange:

* netlib, http://www.netlib.org
* Function parser, http://warp.povusers.org/FunctionParser/
* TCLAP, http://tclap.sourceforge.net/
* genrand, http://www.math.sci.hiroshima-u.ac.jp/m-mat/MT/VERSIONS/C-LANG/980409/mt19937-1.c
