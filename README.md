## Building from source

For Debian based systems:
```
> sudo apt-get install g++ make gfortran libgd-dev doxygen
> make
```
The executable `mfrange` is located in the folder `cl`. Place a copy in your `PATH`, e.g. `/usr/local/bin` or `~/bin`.
