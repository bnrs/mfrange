CC       = gcc
CCFLAGS  = -Wall -O2 -Wno-deprecated -Wno-delete-non-virtual-dtor
XX       = g++
F77      = f77 
F77FLAGS = -Wall -O2 -Wno-line-truncation -Wno-maybe-uninitialized

COMPILE   = $(CC) -c $(DFLAGS) $(WFLAGS) $(OFLAGS) $(IFLAGS)
XXCOMPILE = $(XX) $(DFLAGS) $(WFLAGS) $(OFLAGS) $(IFLAGS)

ALL: all

