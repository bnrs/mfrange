// xy2txt.cc

#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char *argv[])
{
    if ( argc != 2 )
    {
        cerr << "Usage: " << argv[0] << " XY_FILE" << endl;
        exit(1);
    }

    ifstream xy(argv[1]);
    if (xy)
    {
        float x[2];
        while ( xy.good() )
        {
            xy.read((char *)&x, 2*sizeof(float));
            if ( xy.good() )
                cout << x[0] << " " << x[1] << endl;
        }
        xy.close();
    }
    else
    {
        cerr << "Error opening " << argv[1] << endl;
        exit(1);
    }
    
    return 0;
}
