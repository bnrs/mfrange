// MFCompanion.hh

#ifndef __MFCompanion_hh
#define __MFCompanion_hh

#include <string>
#include <set>
#include <MFPolynomial.hh>

/// This class is obsolete und should not be used anymore.
/// To be honest: I don't know why I wrote it anymore...

class MFCompanion : public MFPolynomial
{
    public:
        MFCompanion(const MatrixPolynomial &P);
        virtual const std::string type() const;
};


#endif
