// Domain.hh

#ifndef __Domain_hh
#define __Domain_hh

#include <iostream>
#include <set>
#include <config.hh>

///...
class Box
{
    public: 
        Box(const double &xmin, const double &xmax, const double &ymin, const double &ymax);

        const bool operator<(const Box &other) const;
        const bool has_interior() const;
        const bool contains(const COMPLEX &z) const;

        std::set< Box > remove(const Box &b) const;

        const double max_side() const;
        const COMPLEX middle() const;
        double xmin, xmax, ymin, ymax;

};

std::ostream &operator<<(std::ostream &out, const Box &b);

///...
class Domain : public std::set< Box >
{
    public:
        Domain();
        Domain(const Box &b);
        Domain(const std::set< Box > &b);

        Domain &add(const Box &b);
        Domain &add(const Domain &d);

        Domain &remove(const Box &b);
        Domain &remove(const Domain &d);

        const bool contains(const COMPLEX &z) const;
};

std::ostream &operator<<(std::ostream &out, const Domain &d);

#endif
