#ifndef __GENRAND_H
#define __GENRAND_H

extern "C"
{
  double genrand();
  void sgenrand(unsigned long);
}

#endif
