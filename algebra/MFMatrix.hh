// MFMatrix.hh

#ifndef __MFMatrix_hh
#define __MFMatrix_hh

#include <string>
#include <vector>
#include <ComplexMatrix.hh>
#include <MatrixFunction.hh>

/// The most simple version of a matrix function: \f$\F(z) = A - z\f$.

class MFMatrix 
:   public MatrixFunction,
    public ComplexMatrix
{
    public:
        MFMatrix(const ComplexMatrix &A);
        virtual const std::string type() const;
        void write(std::ostream &out) const;
        const int dimension() const;
        const ComplexMatrix eval(const COMPLEX &z);
        MatrixFunction *sub_function(const std::set< int > &K);
        const std::vector< COMPLEX > block_eigenvalues
            (const std::vector< std::set< int > > D,
             const std::vector< ComplexVector > &x, const std::vector< ComplexVector > &y);
        const std::vector< COMPLEX > eigenvalues();
};

#endif
