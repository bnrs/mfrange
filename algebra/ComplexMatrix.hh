// ComplexMatrix.hh

#ifndef __ComplexMatrix_hh
#define __ComplexMatrix_hh

#include <iostream>
#include <set>
#include <Matrix.hh>
#include <ComplexVector.hh>

/// Very basic class representing complex \f$n \times m\f$ matrices.
/**
\note
    - This is not a general purpose matrix class; it only provides
      methods needed for the calculations of BNRs.
    - The matrix is assumed to have the entries
      \f[
      \left(\begin{array}{ccc}
        a_{00} & \cdots & a_{0,m-1} \\
        \vdots &        & \vdots \\
        a_{n0} & \cdots & a_{n-1,m-1} \\
      \end{array}\right)
      \f]
    - 0-dimensional matrices are allowed
*/

class ComplexMatrix : public Matrix<COMPLEX>
{
    public:

        DEFINE_MATRIX_CONSTRUCTORS(ComplexMatrix,COMPLEX);

        /// The \f$n \times n\f$ identity matrix
        static ComplexMatrix id(const int &n);

        /// The \f$n \times n\f$ zero matrix
        static ComplexMatrix zero(const int &n);

/// @name Convenience
//@{
        /// The \p rows() \p x \p rows() identity matrix
        const ComplexMatrix id() const;

        /// The \p rows() \p x \p columns() zero matrix
        const ComplexMatrix zero() const;
//@}

/// @name Addition/Subtraction
//@{
        const ComplexMatrix operator +=(const ComplexMatrix &other);
        const ComplexMatrix operator +(const ComplexMatrix &other) const;
        const ComplexMatrix operator -=(const ComplexMatrix &other);
        const ComplexMatrix operator -(const ComplexMatrix &other) const;
//@}

/// @name Scalar multiplication
//@{
        const ComplexMatrix operator *=(const COMPLEX &z);
        const ComplexMatrix operator *(const COMPLEX &z) const;
//@}

/// @name Functions
//@{
        /// Apply the matrix to a vector \p v
        const ComplexVector operator *(const ComplexVector &v) const;

        /// Matrix multiplication
        const ComplexMatrix operator *(const ComplexMatrix &other) const;

        /// The determinant of a quadratic matrix
        const COMPLEX det() const;

        /// A vector representing the eigenvalues of the matrix
        const std::vector< COMPLEX > eigenvalues() const;
        
        /// The generalized eigenvalues (\f$Ax = \lambda Bx\f$)
        const std::vector< COMPLEX > eigenvalues(const ComplexMatrix &B) const;

        /// The operator norm of the matrix: \f$\|A\| = \max |\sigma(A^*A)|\f$.
        const DOUBLE norm() const;

        /// The (complex) adjoint \f$A^* = \overline{A}^{\text{t}}\f$ of a matrix.
        const ComplexMatrix adjoint() const;

        /// The inverse of a quadratic matrix
        const ComplexMatrix inverse() const;

        /// Return the block scalar product with respect to \p x
        /** ... */
        const ComplexMatrix block_sp(const std::vector< ComplexVector > &x) const;

        /// The case \p D.is_empty() is treated like the case \f$D = \{1,\dots,n\}\f$.
        /// Then \p x must contain exactly one element.
        const ComplexMatrix block_sp(const std::vector< std::set< int > > &decomposition,
                                     const std::vector< ComplexVector > &x,
                                     const std::vector< ComplexVector > &y) const;

        const ComplexMatrix block_sp(const std::vector< std::set< int > > &decomposition,
                                     const std::vector< ComplexVector > &x) const;

        /// ...
        const ComplexMatrix schur_complement(const int &k1, const int &k2) const;

        /// ...
        const ComplexMatrix schur_complement(const std::set< int > &K) const;

//@}


    private:
        static long ev_fiter, ev_fZERO, ev_fnan, ev_finfo;

    public:
        static const long &ev_f_iter() { return ev_fiter; }
        static const long &ev_f_ZERO() { return ev_fZERO; }
        static const long &ev_f_nan() { return ev_fnan; }
        static const long &ev_f_info() { return ev_finfo; }
};

#endif
