FROM ubuntu:20.04 AS build
RUN apt-get update 
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get install -y g++ make gfortran libgd-dev doxygen
WORKDIR /src
COPY . .
RUN make

FROM ubuntu:20.04
RUN apt-get update
RUN apt-get install -y libgd3
COPY --from=build /src/cl/mfrange /usr/bin/mfrange
ENTRYPOINT [ "/usr/bin/mfrange" ]
