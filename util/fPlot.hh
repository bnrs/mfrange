/*
  file: fPlot.h
*/  

#ifndef __fPlot_hh
#define __fPlot_hh

#include <string>
#include <complex>
#include <vector>
#include <gd.h>

/// ...

class fPlot {
    public:
        /// A new image based on the parameters
        fPlot(const float &xmin, const float &xmax, 
              const float &ymin, const float &ymax,
              const unsigned &width = 0, const unsigned &height = 0,
              const bool &xnumbers = true, const bool &ynumbers = true,
              const bool &draw_grid = false);

        fPlot(const fPlot &other);

        /// Load an image from a file
        /**
            - Take care that \p xmin etc. match the ones from the image
            loaded!
            - Coordinates, x, y-numbers and grid are \e not drawn anymore
        */
        fPlot(const float &xmin, const float &xmax, 
              const float &ymin, const float &ymax,
              const std::string &filename);

        ~fPlot();

        const bool ready() const;

        const bool write(const std::string &filename = "");

        const float &get_xmin() const;
        const float &get_xmax() const;
        const float &get_ymin() const;
        const float &get_ymax() const;

        void set_color(const int &red, const int &green, const int &blue);
        
        void set(float x, float y);
        void set(const std::complex<float> &z);
        void set(const std::vector< std::complex<float> > &z);

        int get_red(const float &x, const float &y) const;
        int get_green(const float &x, const float &y) const;
        int get_blue(const float &x, const float &y) const;

        void line(const float &x1, const float &y1, const float &x2, const float &y2);
        void line(const std::complex<float> &z1, const std::complex<float> &z2);

        void rectangle(const float &x1, const float &y1, const float &x2, const float &y2);
        void rectangle(const std::complex<float> &z1, const std::complex<float> &z2);

        void filled_rectangle(const float &x1, const float &y1, const float &x2, const float &y2);
        void filled_rectangle(const std::complex<float> &z1, const std::complex<float> &z2);

        /// A circle of \e pixel (!) radius \p radius on the image
        void circle(const float &x, const float &y, const int &radius, const bool &solid = false);
        void circle(const std::complex<float> &z, const int &radius, const bool &solid = false);
        void circle(const std::vector< std::complex<float> >&z, const int &radius, const bool &solid = false);

        /// A \box of half side length \p size (in pixels!)
        void box(const float &x, const float &y, const int &size, const bool &solid = false);
        void box(const std::complex<float> &z, const int &size, const bool &solid = false);
        void box(const std::vector< std::complex<float> >&z, const int &size, const bool &solid = false);

        /// An 'x' of half side length \p size (in pixels!)
        void cross(const float &x, const float &y, const int &size);
        void cross(const std::complex<float> &z, const int &size);
        void cross(const std::vector< std::complex<float> >&z, const int &size);

        /// A circle in the plane with radius \p radius;
        /// (will appear as an ellipse on the image in general)
        void sphere (const float &x, const float &y, const float &radius, const bool &solid = false);
        void sphere(const std::complex<float> &z, const float &radius, const bool &solid = false);

        void polygon(const std::vector<float> &x, const std::vector<float> &y);
        void polygon(const std::vector< std::complex<float> > &z);

    private:
        gdImagePtr  image;
        int         width,
                    height,
                    left, 
                    right,
                    top, 
                    bottom,
                    DH,                 // = right - left
                    DV,                 // = bottom - top
                    current_color;
        float       xmin, xmax, ymin, ymax,
                    dx, dy;
        bool        draw_grid,
                    xnumbers, ynumbers;
        int         black,
                    white,
                    lightgray,
                    green;

        void draw_coordinates();

        const int getiX(const float &x) const;
        const int getiY(const float &y) const;

        const float getfX(const int &ix) const;
        const float getfY(const int &iy) const;

        const int allocate_color(const int &red, const int &green, const int &blue) const;
        const int entier(const float &x) const;

        ///  entier of x corresponding to 10^ord, ie. eg. entier(0.52,-1) = 0.5, entier(-76,1) = -80
        const float entier(const float &x, const int &ord) const;
        const int ceil(const float &x) const;

        static const int DEFAULT_WIDTH  = 600,
                         DEFAULT_HEIGHT = 600,
                         BORDER_LEFT    = 40, 
                         BORDER_RIGHT   = 30,
                         BORDER_TOP     = 30,
                         BORDER_BOTTOM  = 40;
};

#endif
