// MatrixFunction.cc

#include <cassert>
#include <MatrixFunction.hh>
#include <ComplexMatrix.hh>

using namespace std;

MatrixFunction::MatrixFunction()
{
    eval_error = false;
}

void MatrixFunction::set_domain(const Domain &domain)
{
    this->domain = domain;
}

void MatrixFunction::add_domain(const Domain &d)
{
    domain.add(d);
}

const COMPLEX MatrixFunction::eval(const int &i, const int &j, const COMPLEX &z)
{
    return eval(z)(i,j);
}

// virtual
const vector< COMPLEX > MatrixFunction::block_eigenvalues
        (const vector< set< int > > D, 
         const vector< ComplexVector > &x, const vector< ComplexVector > &y)
{
    this->D = D;
    this->x = x;
    this->y = y;
    floatZF zf(this);
    zf.count_in(domain);
    return zf.get_zeros();
}

const vector< COMPLEX > MatrixFunction::block_eigenvalues
        (const vector< set< int > > D, const vector< ComplexVector > &x)
{
    return block_eigenvalues(D, x, x);
}

// virtual
const vector< COMPLEX > MatrixFunction::eigenvalues()
{
    x.clear();
    y.clear();
    floatZF zf(this);
    zf.count_in(domain);
    return zf.get_zeros();
}

const COMPLEX MatrixFunction::f(const COMPLEX &z)
{
    if ( x.empty() || y.empty() )
        return eval(z).det();
    else
    {
        ComplexMatrix V = eval(z).block_sp(D, x, y);
        if ( V.is_null() )
        {
            cerr << __FILE__ << ", " << __LINE__ << ": V is NULL" << endl;
            return 0.0;
        }
        return V.det();
    }
}
