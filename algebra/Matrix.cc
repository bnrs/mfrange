// Matrix.cc

#include "Matrix.hh"
#include <string>
#include <config.hh>

using namespace std;

template <> 
void Matrix< string >::to_zero(string &t)
{
    t = "";
}

template <> 
void Matrix< COMPLEX >::to_zero(COMPLEX &t)
{
    t = 0.0;
}


