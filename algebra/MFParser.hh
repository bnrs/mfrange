// MFParser.hh

#ifndef __MFParser_hh
#define __MFParser_hh

#include <string>
#include <vector>
#include <MatrixFunction.hh>
#include <Matrix.hh>
#include <cfparser.hh>

/// A MatrixFunction whose entries are parsed from strings

class MFParser 
:   public MatrixFunction
{
    public:
        MFParser(const Matrix< std::string > &fs);
        virtual const std::string type() const;
        void write(std::ostream &out) const;
        const int dimension() const;
        const ComplexMatrix eval(const COMPLEX &z);
        const COMPLEX eval(const int &i, const int &j, const COMPLEX &z);
        MatrixFunction *sub_function(const std::set< int > &K);
        const Matrix< std::string > &get_fs() const { return fs; }
        const std::string &get_parse_error() const { return parse_error; }

    private:
        Matrix< FunctionParser > fp;
        Matrix< std::string > fs;
        bool eval_error;
        std::string parse_error;
};

#endif
