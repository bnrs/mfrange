// MFString.cc

#include <cassert>
#include <MFParser.hh>
#include <ComplexMatrix.hh>
#include <util.hh>

using namespace std;

template <> 
void Matrix<FunctionParser>::to_zero(FunctionParser &fp)
{
    fp = FunctionParser();
}

MFParser::MFParser(const Matrix< std::string > &fs)
{
    this->fs = fs;
    int dim = fs.dimension();

    fp = Matrix< FunctionParser >(dim);
    for ( int i = 0; i < dim; ++i )
        for ( int j = 0; j < dim; ++j )
    {
        int e = fp(i,j).Parse(fs(i,j),"z");
        if ( e != -1 )
        {
            using namespace util;
            parse_error += 
                string("(") + to_string(i) + "," + to_string(j) + "), \"" +
                fs(i,j) + "\": character " + to_string(e) + " ('" + fs(i,j)[e] + "').\n";
        }
    }
}

const std::string MFParser::type() const
{
    return "Parser";
}

const int MFParser::dimension() const 
{
    return fp.dimension();
}


const ComplexMatrix MFParser::eval(const COMPLEX &z)
{
    static ComplexMatrix V(dimension());

    for ( int i = 0; i < dimension(); ++i )
        for ( int j = 0; j < dimension(); ++j )
        {
            V(i,j) = eval(i, j, z);
            if ( eval_error )
                return ComplexMatrix();
        }

    return V;
}

const COMPLEX MFParser::eval(const int &i, const int &j, const COMPLEX &z)
{
    assert ( 0 <= i && i < dimension() && 0 <= j && j < dimension() );
    int k = i*dimension() + j;
    COMPLEX v, w[1];
    w[0] = z;

    eval_error = false;
    v = fp[k].Eval(w);
    eval_error = fp[k].EvalError();

    return v;
}

MatrixFunction *MFParser::sub_function(const set< int > &K)
{
    MatrixFunction *r = new MFParser(fs.sub(K));
    r->add_domain(get_domain());
    return r;    
}
