// MatrixFunctionIO.cc

#include <fstream>
#include <MFMatrix.hh>
#include <MFPolynomial.hh>
#include <MFParser.hh>
#include <MFSchurComplement.hh>

using namespace std;

// static 
MatrixFunction *MatrixFunction::read(std::istream &in)
{
    MatrixFunction *F = NULL;

    char t;
    in >> t;

    if ( in.bad() )
        return F;

    if ( t == 'M' )
    {
        int dim;
        in >> dim;
        
        if ( in.bad() )
            return F;

        ComplexMatrix A;
        if ( dim > 0 )
        {
            A = ComplexMatrix(dim);
            in >> A;
        }

        if ( in.good() )
            F = new MFMatrix(A);
    }
    else if ( t == 'P' )
    {
        int deg, dim;
        in >> deg >> dim;

        if ( in.bad() )
            return F;

        vector< ComplexMatrix > A;
        for ( int k = 0; k <= deg; ++k )
        {
            ComplexMatrix tmp(dim);
            in >> tmp;
            if ( in.good() )
                A.push_back(tmp);
            else
                break;
        }

        if ( in.good() )
            F = new MFPolynomial(MatrixPolynomial(A));
    }
    else if ( t == 'F' )
    {
        int dim;
        in >> dim;

        if ( in.bad() )
            return F;

        Matrix< string > fs;
        if ( dim > 0 )
        {
            fs = Matrix< string >(dim);
            in >> fs;
        }

        if ( in.good() )
        {
            F = new MFParser(fs);

            int dn;
            in >> dn;

            if ( dn >= 0 && in.good() && !in.eof() )
                for ( int k = 0; k < dn; ++k )
                {
                    float xmin, xmax, ymin, ymax;
                    in >> xmin >> xmax >> ymin >> ymax;
                    if ( in.good() )
                        F->add_domain(Box(xmin,xmax,ymin,ymax));
                }
        }
    }

    return F;
}

// static
MatrixFunction *MatrixFunction::read(const std::string &filename)
{
    std::ifstream in(filename.c_str());
    if (!in)
        return NULL;

    return MatrixFunction::read(in);
}

std::ostream &operator << (std::ostream &out, const MatrixFunction &F)
{
    if ( F.type() == "Matrix" )
    {
        out << "M ";
        F.write(out);
    }
    else if ( F.type() == "Polynomial" )
    {
        out << "P ";
        F.write(out);
    }
    else if ( F.type() == "Parser" )
    {
        out << "F ";
        F.write(out);
    }
    else if (F.type() == "Schur-complement")
    {
        F.write(out);
    }

    if ( F.get_domain().size() > 0 )
        out << F.get_domain().size() << endl 
            << F.get_domain();

    return out << flush;
}


void MFMatrix::write(ostream &out) const
{
    out << dimension() 
        << (*(ComplexMatrix *)(this));
}

void MFPolynomial::write(ostream &out) const
{
    out << degree() << " " <<  dimension() << endl 
        << (*(MatrixPolynomial *)(this));
}

void MFParser::write(ostream &out) const
{
    out << dimension() << endl 
        << fs;
}

void MFSchurComplement::write(ostream &out) const
{
    set< int >::const_iterator i = K.begin();
    out << ((*i) + 1);
    for ( ++i; i != K.end(); ++i )
        out << "," << ((*i) + 1);
    out << "-Schur complement of the matrix function\n---\n"
        << (*F) << "---\n"
        << "Removed singularities:\n" << singularities << endl;
}
