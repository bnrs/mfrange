// config.hh

#ifndef __config_hh
#define __config_hh

#include <complex>
#define DOUBLE double
#define COMPLEX std::complex< DOUBLE >

#define PI 3.1415926535897932384626433832795029L
#define TWO_PI (2.0*PI)

#define FLOATZF_MAX_RECT      1
#define FLOATZF_LINE_LENGTH   0
#define FLOATZF_PRECISION     1e-3
#define FLOATZF_SUBDIVISIONS  3

#define ASSERT(C,MSG) if ( !(C) ) { std::cerr << __FILE__ << ", " << __PRETTY_FUNCTION__ << ", " << __LINE__ << ": Assertion " << #C << " failed.\n" << MSG << std::endl; exit(1); }
#define SHOW_VAR(X) (#X) << " = " << (X)
#define AND_SHOW_VAR(X) << ", " << (#X) << " = " << (X)
#endif
