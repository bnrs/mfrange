// MatrixPolynomial.cc

#include <MatrixPolynomial.hh>
#include <cassert>

using namespace std;

MatrixPolynomial::MatrixPolynomial()
{
    deg = -1;
    C = NULL;
}

MatrixPolynomial::MatrixPolynomial(const vector< ComplexMatrix > &C)
{
    deg = C.size() - 1;

    if ( deg == -1 )
        this->C = NULL;
    else
    {
        this->C = new ComplexMatrix[deg + 1];

        this->C[0] = C[0];
        for ( int k = 1; k <= deg; ++k )
        {
            ASSERT(C[k].rows() == C[0].rows() && C[k].columns() == C[0].columns(),
                   SHOW_VAR(k)AND_SHOW_VAR(C[0])AND_SHOW_VAR(C[k]));
            this->C[k] = C[k];
        }
    }
}

MatrixPolynomial::MatrixPolynomial(const MatrixPolynomial &other)
{
    deg = other.deg;

    if ( deg == -1 )
        C = NULL;
    else
    {
        C = new ComplexMatrix[deg + 1];
        for ( int k = 0; k <= deg; ++k )
            this->C[k] = other.C[k];
    }
}

MatrixPolynomial::~MatrixPolynomial()
{
    delete[] C;
}

const MatrixPolynomial &MatrixPolynomial::operator = (MatrixPolynomial other)
{
    deg = other.deg;
    
    if ( deg == -1 )
        C = NULL;
    else
    {
        C = new ComplexMatrix[deg + 1];
        for ( int k = 0; k <= deg; ++k )
            this->C[k] = other.C[k];
    }
    return *this;
}


const bool MatrixPolynomial::is_empty() const
{
    return deg == -1;
}

const bool MatrixPolynomial::isnt_empty() const
{
    return !is_null();
}

const bool MatrixPolynomial::is_null() const
{
    return is_empty();
}

const bool MatrixPolynomial::isnt_null() const
{
    return !is_null();
}

const bool MatrixPolynomial::is_quadratic() const 
{
    return ( isnt_empty() && C[0].is_quadratic() );
}

const int &MatrixPolynomial::degree() const 
{
    return deg;
}

const int &MatrixPolynomial::rows() const 
{
    ASSERT(isnt_empty(), ""); 
    return C[0].rows();
}

const int &MatrixPolynomial::columns() const 
{
    ASSERT(isnt_empty(), ""); 
    return C[0].columns();
}

const int MatrixPolynomial::dimension() const
{
    ASSERT(isnt_empty(), ""); 
    ASSERT(is_quadratic(), SHOW_VAR(*this));
    return C[0].dimension();
}


const MatrixPolynomial MatrixPolynomial::companion() const
{
    ASSERT(deg > 0, "");
    ASSERT(C[0].isnt_empty(), "");
    
    int dim = dimension();
    int _dim = dim*deg;

    vector< ComplexMatrix > _C;
    _C.push_back(ComplexMatrix(_dim));
    _C.push_back(ComplexMatrix(_dim));

    for ( int i = 0; i < _dim - dim; ++i )
    {
        _C[0](i, i + dim) = -1;
        _C[1](i, i) = 1;
    }

    for ( int i = 0; i < dim; ++i )
        for ( int j = 0; j < deg; ++j )
            for ( int k = 0; k < dim; ++k )
                _C[0](_dim - dim + i,j*dim + k) = C[j](i,k);
    
    for ( int i = 0; i < dim; ++i )
        for ( int j = 0; j < dim; ++j )
            _C[1](_dim - dim + i,_dim - dim + j) = C[deg](i,j);
    
    return MatrixPolynomial(_C);
}

const bool MatrixPolynomial::operator != (const MatrixPolynomial &other) const
{
    if ( deg != other.deg )
        return true;
    
    for ( int k = 0; k <= deg; ++k )
        if ( C[k] != other[k] )
            return true;
    
    return false;
}

const bool MatrixPolynomial::operator == (const MatrixPolynomial &other) const
{
    return !( (*this) != other );
}

const ComplexMatrix &MatrixPolynomial::operator [] (const int &k) const
{
    ASSERT(0 <= k && k <= deg, SHOW_VAR(k)AND_SHOW_VAR(*this));
    return C[k];
}

ComplexMatrix &MatrixPolynomial::operator [] (const int &k)
{
    ASSERT(0 <= k && k <= deg, SHOW_VAR(k)AND_SHOW_VAR(*this));
    return C[k];
}

const MatrixPolynomial MatrixPolynomial::operator()(const set< int > &r, const set< int > &c) const
{
    ASSERT(isnt_empty(), "");
    vector< ComplexMatrix > D;
    
    for ( int k = 0; k <= deg; ++k )
        D.push_back(C[k].sub(r,c));

    return MatrixPolynomial(D);
}

const MatrixPolynomial MatrixPolynomial::sub(const set< int > &r, const set< int > &c) const
{
    return (*this)(r,c);
}

const vector< COMPLEX > MatrixPolynomial::eigenvalues()
{
    ASSERT(deg > 0, SHOW_VAR(*this));
    ASSERT(is_quadratic(), SHOW_VAR(*this));

    int n = dimension();

    if ( n == 0 )
        return vector<COMPLEX>();

    ComplexMatrix A(deg*n), B(deg*n); 

    // solve the generalized eigenvalue problem C_0 y = \lambda C_1 y,
    // where C_1 z - C_0 is the companion polynomial of A
    for ( int i = 0; i < n; ++i )
        for ( int k = 0; k < deg; ++k )
            for ( int j = 0; j < n; ++j )
                A((deg - 1)*n + i, k*n + j) = -C[k](i,j);

    for ( int i = 0; i < (deg - 1)*n; ++i )
    {
        A(i,i + n) = 1.0;
        B(i,i) = 1.0;
    }

    for ( int i = 0; i < n; ++i )
        for ( int j = 0; j < n; ++j )
            B((deg - 1)*n + i, (deg - 1)*n + j) = C[deg](i,j);

    return A.eigenvalues(B);
}

const ComplexMatrix MatrixPolynomial::eval(const COMPLEX &z) const
{
    ASSERT(isnt_empty(), "");

    ComplexMatrix R(rows());
    COMPLEX pz = 1.0;

    for ( int k = 0; k < degree(); ++k, pz *= z )
        R += (*this)[k]*pz;

    return R;
}

MatrixPolynomial MatrixPolynomial::block_sp(const vector< ComplexVector > &x)
{
    vector< ComplexMatrix > A;
    for ( int k = 0; k <= deg; ++k )
        A.push_back(C[k].block_sp(x));
    
    return MatrixPolynomial(A);
}

MatrixPolynomial MatrixPolynomial::block_sp
        (const vector< set< int > > D, 
         const vector< ComplexVector > &x, const std::vector< ComplexVector > &y)
{
    ASSERT(isnt_empty(), "");
    ASSERT(C[0].is_quadratic(), SHOW_VAR(*this));

    vector< ComplexMatrix > A;
    for ( int k = 0; k <= deg; ++k )
        A.push_back(C[k].block_sp(D, x, y));
    
    return MatrixPolynomial(A);
}

MatrixPolynomial MatrixPolynomial::block_sp
        (const vector< set< int > > D, const vector< ComplexVector > &x)
{
    ASSERT(isnt_empty(), "");
    ASSERT(C[0].is_quadratic(), SHOW_VAR(*this));

    return block_sp(D, x, x);
}

std::ostream &operator << (std::ostream &out, const MatrixPolynomial &P)
{
    if ( P.is_empty() )
        return out << "(null)" << flush;
    
    for ( int k = 0; k <= P.degree(); ++k )
        out << P[k];
    
    return out << flush;
}
