// Ranges.hh

#ifndef __Ranges_hh
#define __Ranges_hh

#include <vector>
#include <string>
#include <config.hh>
#include <MatrixFunction.hh>

/// \p Ranges provides several types of (numerical) ranges for matrix functions

/** By now, the following are implemented:
    - Eigenvalues
    - Block numerical range
    - Block determinant set with respect to \f$\lambda \in \C\f$
*/

class Ranges
{
    public:
        Ranges();
        Ranges(MatrixFunction *F, ComplexMatrix *J = NULL);
        ~Ranges();

        /// ...
        void set_decomposition(const std::vector< std::set< int > > decomposition);

        /// Set the matrix function to be used
        void set_matrix_function(MatrixFunction *F);

        /// Set the J-matrix to be used
        void set_J(ComplexMatrix *J);

        /// A pointer to the matrix function which is used
        MatrixFunction *get_matrix_function();

        /// Reads a matrix function from file \p filename
        static MatrixFunction *read_matrix_function(const std::string &filename);

        /// Returns \f$\sgp(\FF_x)\f$ for \p resolution \f$x \in \S^n\f$
        std::vector< COMPLEX > bnr(const int &resolution);

        /// Returns \f$\det\FF_x(z)\f$ for \p resolution \f$x \in \S^n\f$
        std::vector< COMPLEX > bds(const COMPLEX &z, const int &resolution);

        /// Returns the eigenvalues of \f$\F\f$
        std::vector< COMPLEX > ev();

        /// 'Gershgorin Block Numerical Range'.
        /// Returns elements of the numerical ranges of the diagonal entries
        /// Stores \f$1/p_M\sum_{N\ne M}p_N\|A_{MN}\|\f$ to \p R,
        /// \f$p_M\sum_{N\ne M}\|A_{NM}\|/p_N\f$ to \p C and corresponding
        /// values to \p r, \p c on the \f$x \in \S^n\f$-level ...
        std::vector< COMPLEX > gbnr(const int &resolution, const std::vector<DOUBLE> &p, 
                                    std::vector<DOUBLE> &r, std::vector<DOUBLE> &c,
                                    std::vector<DOUBLE> &R, std::vector<DOUBLE> &C);

        std::vector< COMPLEX > bdsbnr(const DOUBLE &xmin, const DOUBLE &xmax, const int &xres,
                                      const DOUBLE &ymin, const DOUBLE &ymax, const int &yres,
                                      const DOUBLE &eps, const int &max_iter);

        void info(); /// NOT defined in Ranges.cc. Define somewhere else!

        static bool handle_signals;

    private:
        MatrixFunction *F;
        ComplexMatrix *J;
        std::vector< std::set< int > > decomposition;
        
        void _init();

        /// Returns a random \f$x = (x_1,\dots,x_n)\f$
        std::vector< ComplexVector > random_block_normed();

        /// Applies J to \p x.
        std::vector< ComplexVector > _J(const std::vector< ComplexVector > &x);

        /// ...
        ComplexVector random_normed(const int &dim);
        
        static bool leave,
                    print_state;
        
        static void enable_signal_handler(const bool &initial = true);
        static void disable_signal_handler();
        static void signal_handler(int sig);

        void show_state(const int &i, const int &res, std::ostream &out = std::cout);
};

#endif
