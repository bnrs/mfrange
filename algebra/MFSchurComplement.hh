// MFSchurComplement.hh

#ifndef __MFSchurComplement_hh
#define __MFSchurComplement_hh

#include <string>
#include <set>
#include <MatrixFunction.hh>

/// The most simple version of a matrix function: \f$\F(z) = A - z\f$.

class MFSchurComplement : public MatrixFunction
{
    public:
        MFSchurComplement(MatrixFunction *F, const std::set< int > &K);
        virtual const std::string type() const;
        void write(std::ostream &out) const;
        const int dimension() const;
        const ComplexMatrix eval(const COMPLEX &z);
        MatrixFunction *sub_function(const std::set< int > &K);
        const Domain &get_singularities() const;
        const std::set< int > &get_K() const;
        const MatrixFunction *get_F() const;

    protected:
        MatrixFunction *F;
        std::set< int > K;
        Domain singularities;
};


#endif
