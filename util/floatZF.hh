// floatZF.hh

#ifndef __floatZF_hh
#define __floatZF_hh

#include <vector>
#include <config.hh>
#include <Domain.hh>

/// Locate zeros and poles of a meromorphic function \f$f\f$ using the argument principle

/**
The number of zeros and poles inside a rectangular domain \f$D\f$ is determined
by the formula
\f[
N_D(f) - P_D(f)
=
\sum_{i = 1}^n \arg(f(z_i)) - \arg(f(z_{i-1}),
\f]
where \f$z_i \in \partial D\f$ such that \f$\partial D\f$ is the disjoint 
union of the line segments \f$[z_{i-1},z_i]\f$ joining \f$z_{i-1}\f$ and \f$z_i\f$,
\f$z_n = z_0\f$, and \f$0 \notin f([z_{i-1},z_i])\f$.
The latter is simulated by the code by subdividing \f$\partial D\f$ until
\f$|z_i - z_{i-1}| < \f$ \p line_length.
Given a rectangular \p Domain (or a \p vector of them) \p D, the call \p count_in(D) determines
\f$N_D(f) - P_D(f)\f$, and, if \f$N_D(f) - P_D(f) \neq 0\f$, recursively
subdivides \p D until the desired \p precision is reached.
One may access them by the calls \p get_zeros() and \p get_poles(). 

\note 
- If \f$D\f$ contains the same number of zeros and poles the
code will claim there are no zeros/poles inside \f$D\f$ --
and consequently not subdivide \f$D\f$!
So, take care in the choice of suitable domains ...
- By now, if \f$f(z_i) = 0\f$ for some \f$z_i\f$, the code will
simply \em fail (in \p arg_diff()).
*/

class floatZF
{
    public:
    
        /// Access to the meromorphic function \f$f\f$
        class Host
        {
            public:
                virtual ~Host() {}
                virtual const COMPLEX f(const COMPLEX &z) = 0;
        };

    public:
        /// \p fh must be a child of \p Host, implementing the meromorphic function \f$f\f$
        floatZF(Host *fh);

        /// Set \p line_length
        void set_line_length(const double &l) { line_length = l; }

        /// Set the \p precision
        void set_precision(const double &p) { precision = p; }

        /// Remove all zeros and poles found by now
        void clear();

        /// Count zeros/poles inside the box \p b
        int count_in(const Box &b);

        /// Count zeros/poles inside the domain \p d
        void count_in(const Domain &d);

        /// Get all zeros found since the previous \p clear()
        const std::vector< COMPLEX > &get_zeros() const { return zeros; } 

        /// Get all poles found since the previous \p clear()
        const std::vector< COMPLEX > &get_poles() const { return poles; } 

        /// ...
        static void reset_E_WRONG_COUNT() { E_WRONG_COUNT = 0; }

        /// ...
        static long get_E_WRONG_COUNT() { return E_WRONG_COUNT; }

        /// ...
        static void set_config(const double &max_rect, const double &line_length, const double &precision,
                               const int &subdivisions);

        static bool DEBUG;

    protected:

        std::vector< COMPLEX > zeros;
        std::vector< COMPLEX > poles;
        Host *fh;

        static double 
            max_rect,
            line_length,
            precision;

        static int subdivisions;

        double line_length_x, 
               line_length_y;

        const double count_line(const COMPLEX &z1, const COMPLEX &z2) const;
        const double arg_diff(const COMPLEX &v2, const COMPLEX &v1) const;

        static long E_WRONG_COUNT;
};

/** \example fZF.cc */

#endif
